import _ from 'lodash';

export const getFormErrors = state =>
  _.get(state, ['form', 'requestFormStepOne', 'syncErrors'], false);