import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm, FieldArray, Field, formValueSelector, change } from 'redux-form';
import { LIBRARY_SHARE_FORM } from './../constants';
import { hasSyncErrors } from './../selectors';
import AssetSelector from './asset-selector';
import AssetSelectorReview from './asset-selector-review';
import Recipients from './recipients';
import validate from './../validate';
import { submitShareForm, setShareFormStep } from './../actions';
import LoadingComponent from '../../../components/loading'

class ResourceShare extends React.Component {
  constructor(props) {
    super(props);
    this.state = { currentStep: 1 };
    this.removeAssetHandler = this.removeAssetHandler.bind(this);
  }
  renderStepOne() {
    const { assets, assetType, selectedCount, setAssetValue, currentStep, setCurrentStep } = this.props;
    const handleSelectAll = value => {
      setAssetValue(assets.map(i => ({
        ...i,
        isSelected: value
      })))
    }
    return (
      <div className="step-one">
        <div className="image-selector">
          <label htmlFor="select-all-assets" className="select-all">
            <Field component="input"
              type="checkbox"
              id="select-all-assets"
              name="isSelectAll"
              onChange={(e, value) => handleSelectAll(value)} />
            <span className="checkbox icon-check"></span>
            <span>Select All</span>
          </label>
          <FieldArray name="assets" component={AssetSelector} allAssets={assets} assetType={assetType} />
        </div>
        <div className="page-footer">
          {
            selectedCount > 0 ?
              <div className="file-count">{selectedCount} File{selectedCount > 1 ? 's' : ''} Selected</div> :
              <div className="file-count">Select File(s)</div>
          }
          <button className="next icon-arrow-right" disabled={selectedCount === 0} onClick={() => setCurrentStep(currentStep + 1)}>Next</button>
        </div>
      </div>
    )
  }
  removeAssetHandler(index) {
    const field = `assets[${index}].isSelected`;
    this.props.unselectAsset(field);
  }
  renderStepTwo() {
    const { assets, hasError, currentStep, setCurrentStep } = this.props;
    return (
      <div className="step-two">
        <div className="form-wrapper">
          <h3>{'You\'ve selected the following files to share:'}</h3>
          <FieldArray name="assets"
            component={AssetSelectorReview}
            removeHandler={this.removeAssetHandler}
            allAssets={assets} />
          <h3>Send files to:</h3>
          <FieldArray name="recipients" component={Recipients} />
          <Field
            name="comments"
            type="textarea"
            component="textarea"
            label="Comments"
            placeholder="Comments" />
        </div>
        <div className="page-footer">
          <button className="previous icon-arrow-left" onClick={() => setCurrentStep(currentStep - 1)}>Previous</button>
          <button className="submit icon-send" disabled={!hasError} type="submit" >Send</button>
        </div>
      </div>
    )
  }
  renderStepSent() {
    return (
      <div className="main success"><h2 className="confirmation">Files Sent Successfully!</h2></div>
    )
  }
  componentWillUnmount() {
    this.props.setCurrentStep(1);
  }
  render() {
    const { handleSubmit, submitForm, currentStep, isSubmitting } = this.props;
    if(isSubmitting){
      return <LoadingComponent/>
    }
    return (
        <form onSubmit={handleSubmit(submitForm)}>
          {currentStep === 1 ? this.renderStepOne() : currentStep === 2 ? this.renderStepTwo() : this.renderStepSent()}
        </form>
    );
  }
}

ResourceShare.propTypes = {
  initialValues: PropTypes.object,
  isSelectAll: PropTypes.bool
};
const mapStateToProps = (state) => {
  const selector = formValueSelector(LIBRARY_SHARE_FORM)
  return {
    isSelectAll: selector(state, 'isSelectAll'),
    assets: selector(state, 'assets'),
    selectedCount: selector(state, 'assets').filter(i => i.isSelected).length,
    hasError: hasSyncErrors(state),
    isSubmitting: state.library.isSubmittingShareForm,
    currentStep: state.library.shareFormStep
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setAssetValue: (assets) => dispatch(change(LIBRARY_SHARE_FORM, 'assets', assets)),
    unselectAsset: (field) => dispatch(change(LIBRARY_SHARE_FORM, field, false)),
    submitForm: () => dispatch(submitShareForm()),
    setCurrentStep: step => dispatch(setShareFormStep(step))
  };
};
const connected = connect(mapStateToProps, mapDispatchToProps)(ResourceShare);

const formed = reduxForm({
  form: LIBRARY_SHARE_FORM,
  enableReinitialize: true,
  destroyOnUnmount: true,
  validate
})(connected);

export default formed;
