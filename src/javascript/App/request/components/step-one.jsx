import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import RecipientInfoEdit from './recipient-info-edit';
import { Field, FieldArray } from 'redux-form';
import { requestResouces } from '../actions';
import { renderSelect } from '../../../helpers/form-helpers';
import ResourceSelector from './resource-selector';
import { getResourceTypes, getSelectedType } from '../selectors';

class StepOne extends React.Component{
  render(){
    const { options, resourceTypes, selectedType, requestResouces } = this.props;
    return (
      <div className="main">
      	<div className="field-wrapper">
      		<h3>To submit a sample request by email, please download <a href="http://connect.tecturadesigns.com/wp-content/uploads/2018/06/Master-2018-Sample-Request-Sheet-All-Divisions-Combined.xlsx">this</a> spreadsheet and send it to <a href="mailto:wtile@wausautile.com">wtile@wausautile.com</a>.</h3>
      		<hr/>
      	</div>
        <div className="field-wrapper">
          <h3>Who is your request for?</h3>
          <Field
            name="requestFor"
            component="select"
            label="Options"
            component={renderSelect}>
            <option value="" disabled>Select one</option>
            {
              options && options.map(option => {
                return (
                  <option key={option.value} value={option.value}>
                    {option.name}
                  </option>
                );
              })
            }
          </Field>
        </div>
        <div className="field-wrapper">
          <h3>What type of request is this?</h3>
          <Field
            name="resourceType"
            component="select"
            label="Types"
            component={renderSelect}
            onChange={e => requestResouces(e.target.value)}>
            <option value="" disabled>Select one</option>
            {
              resourceTypes && resourceTypes.map(type => {
                return (
                  <option key={type.slug} value={type.id}>
                    {type.name}
                  </option>
                );
              })
            }
          </Field>
        </div>
        <div className="field-wrapper">
          <h3>Recipient Information</h3>
          <FieldArray name="recipients" component={RecipientInfoEdit} />
        </div>
        {
          selectedType &&
          <div className="field-wrapper">     
	       
            <h3>Select <span>{selectedType.name}</span></h3>
            <div>
              <FieldArray name="resources" component={ResourceSelector} />
              {/* {resources && <ResourceSelector resourceType={selectedType.id} resources={resources} />} */}
            </div>
          </div>
        }
      </div>
    );
  }
}

StepOne.propTypes = {
  options: PropTypes.array,
  resouceTypes: PropTypes.array,
  requestFormData: PropTypes.func,
  requestResouces: PropTypes.func,
  selectedType: PropTypes.object,
};
const mapStateToProps = state => {
  return {
    options: state.request.options,
    resourceTypes: getResourceTypes(state),
    selectedType: getSelectedType(state),
  };
};

export default connect(mapStateToProps, { requestResouces })(StepOne);