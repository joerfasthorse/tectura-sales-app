export const LOGIN_FORM = 'LOGIN_FORM';
export const LOGIN_REQUEST = 'login/login-request';
export const LOGIN_SUCCESS = 'login/login-success';
export const LOGIN_FAILURE = 'login/login-failure';

export const LOGOUT_REQUEST = 'login/logout-request';
export const LOGOUT_SUCCESS = 'login/logout-success';

export const CHECK_AUTHENTICATION = 'login/check-authentication';
export const VALIDATE_TOKEN = 'loging/validate-token';