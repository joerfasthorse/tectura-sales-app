import { 
  LOGIN_REQUEST,
  LOGOUT_REQUEST,
  CHECK_AUTHENTICATION
 } from './constants';

export const requestLogin = payload => ({
  type: LOGIN_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    payload
})

export const requestLogout = payload => ({
  type: LOGOUT_REQUEST,
  isFetching: true,
  isAuthenticated: true,
  payload
})

export const checkAuthentication = () => ({
  type: CHECK_AUTHENTICATION
})