import { takeLatest, call, put} from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { loginUser, validateToken } from './api';
import { stopSubmit, startSubmit } from 'redux-form'
import { LOGIN_FORM } from './constants';
import { requestLogout } from './actions';
// Our login constants
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  CHECK_AUTHENTICATION,
  VALIDATE_TOKEN
} from './constants';

// eslint-disable-next-line
function* loginSaga({ payload: { creds, callback } }) {
  yield put( startSubmit( LOGIN_FORM ) );

  try {
    const resp = yield call(loginUser, creds);
    if(resp.data && resp.data.status !== 200 ){
      yield put( stopSubmit( LOGIN_FORM , resp ) );
      yield put({ type: LOGIN_FAILURE, message: resp });
    } else{
      window.localStorage.setItem('auth_user', JSON.stringify(resp));
      yield put({ type: LOGIN_SUCCESS, user: resp });
      yield put(push('/app/'));
    }
    return true;
  } catch (error) {
    // error? send it to redux
    yield put( stopSubmit( LOGIN_FORM , error ) );
    yield put({ type: LOGIN_FAILURE, message: error });
    return false;
  }
}

function* checkAuthenticationSaga() {
  try {
    const isValidate = yield call(validateToken);
    if (isValidate) {
      yield put({ type: VALIDATE_TOKEN })
    } else {
      yield put(requestLogout())
    }

  } catch (error) {
    yield put(requestLogout())
  }
}


// eslint-disable-next-line
function* logoutSaga() {
  let token;
  try {
    yield put({ type: LOGOUT_SUCCESS });
    window.localStorage.removeItem('auth_user');
    yield put(push('/'));
  } catch (error) {
    // error? send it to redux
  }
  // return the token for health and wealth
  return token;
}

// eslint-disable-next-line
function* mySaga() {
  yield takeLatest(LOGIN_REQUEST, loginSaga);
  yield takeLatest(LOGOUT_REQUEST, logoutSaga);
  yield takeLatest(CHECK_AUTHENTICATION, checkAuthenticationSaga);
}

export default mySaga;