import React from 'react';
const RecipientInfoView = ({ recipients }) => {
  return (
    <div className="recipient-info">
      {
        recipients && recipients.map((recipient, index) =>
          <div key={index}>
            {recipient.firstName} {recipient.lastName} <br />
            {recipient.address} <br />
            {recipient.state} {recipient.zipcode} <br />
            {recipient.email} <br />
          </div>
        )
      }
    
    </div>
  );
};

export default RecipientInfoView;
