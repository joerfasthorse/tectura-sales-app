import React from 'react';
import { Field } from 'redux-form';
import { renderInput } from '../../../helpers/form-helpers';

const Recipients = ({ fields }) => {
  return (
    <div>
      {
        fields.map((item, index) => {
          return (

            <div key={index} className="recipient-block">
              {
                fields.length > 1 && <button
                  type="button"
                  title="Remove Recipient"
                  className="remove"
                  onClick={() => fields.remove(index)} >Remove Recipient</button>
              }
              <Field component={renderInput}
                type="email"
                placeholder="email"
                name={`${item}.email`} />
            </div>
          );
        })
      }
      <button type="button" className="add icon-close" onClick={() => fields.push({})}>Add another recipient</button>
    </div>
  );
}
export default Recipients;
