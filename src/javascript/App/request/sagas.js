import { takeLatest, call, put, select } from 'redux-saga/effects';
import { change, reset } from 'redux-form';

import { submitRequest, fetchResourceTypes, fetchResources } from './api';
import { getRequestSubmitData, getSelectedResourceType } from './selectors';
import { setRequestFormStep } from './actions'

// Our login constants
import {
  REQUEST_RESOURCE_TYPES,
  REQUEST_RESOURCE,
  FETCHING_RESOURCE_TYPES,
  FETCHING_RESOURCES,
  RESOURCE_TYPES_SUCCESS,
  RESOURCES_SUCCESS,
  REQUEST_SUBMIT_FORM,
  SUBMITTING_FORM,
  FORM_SUBMIT_SUCCESS,
  REQUEST_FORM_NAMESPACE,
} from './constants';

function* fetchResourceTypesSaga() {
  try {
    yield put({ type: FETCHING_RESOURCE_TYPES });
    const data = yield call(fetchResourceTypes);
    yield put({ type: RESOURCE_TYPES_SUCCESS, data });
  } catch (error) {
    // error? send it to redux
  }
  // return the token for health and wealth
}

function* fetchResourcesSaga({ typeId }) {
  try {
    yield put({ type: FETCHING_RESOURCES });
    const state = yield select();
    // yield put(unregisterField(REQUEST_FORM_NAMESPACE,'selectResources'));
    const resourceType = getSelectedResourceType(state, typeId);
    const data = yield call(fetchResources, resourceType.slug);
    yield put(change(REQUEST_FORM_NAMESPACE, 'resources', mapResources(data)));
    yield put({ type: RESOURCES_SUCCESS, data });
  } catch (error) {
    // error? send it to redux
  }
  // return the token for health and wealth
}

const mapResources = resources => resources.map(r => ({
  id: r.id,
  name: r.slug,
  title: r.title.rendered,
  isSelected: false,
  quanity: 1,
  thumbnail: r.thumbnail ? {
    src: r.thumbnail.guid,
    title: r.thumbnail.post_title
  } : null
}))


function delay(ms) {
  return new Promise(resolve => setTimeout(() => resolve(true), ms))
}

function* submitRequestSaga({ payload: { callback } }) {
  try {
    yield put({ type: SUBMITTING_FORM, data: false });
    const requestData = yield select(getRequestSubmitData);
    const resp = yield call(submitRequest, requestData);
    yield put({ type: FORM_SUBMIT_SUCCESS });
    if (resp) {
      yield put(setRequestFormStep(3))
      yield call(delay, 3000);
      yield put(reset(REQUEST_FORM_NAMESPACE));
      yield put(setRequestFormStep(1))
      callback(resp)
    }

  } catch (error) {
    // error? send it to redux
    callback(false)
  }
  // return the token for health and wealth
}

// eslint-disable-next-line
function* mySaga() {
  yield takeLatest(REQUEST_RESOURCE_TYPES, fetchResourceTypesSaga);
  yield takeLatest(REQUEST_RESOURCE, fetchResourcesSaga);
  yield takeLatest(REQUEST_SUBMIT_FORM, submitRequestSaga);
}

export default mySaga;