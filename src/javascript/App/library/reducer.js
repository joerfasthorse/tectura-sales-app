import {
  REQUEST_LIBRARY_RESOURCE_TYPES,
  REQUEST_LIBRARY_RESOURCES,
  REQUEST_LIBRARY_RESOURCE_BY_ID,
  FETCHING_LIBRARY_DATA,
  SET_LIBRARY_RESOURCE_TYPES,
  SET_LIBRARY_RESOURCES,
  SET_LIBRARY_RESOURCE_VIEWING,
  SUBMITTING_SHARE_FORM,
  SHARE_FORM_SUBMIT_SUCCESS,
  SHARE_FORM_SUBMIT_ERROR,
  SET_LIBRARY_SHARE_FORM_STEP,
  SET_LIBRARY_IS_SHARE_MODE,
  SET_LIBRARY_PRODUCT_TYPES,
  ERROR_LIBRARY_RESOURCE_VIEWING,
  ERROR_LIBRARY_RESOURCES,
  SET_LIBRARY_SELECTED_PRODUCT_TYPE,
  SET_LIBRARY_SELECTED_RESOURCE_TYPE,
  FETCHING_DATA_DONE,
  SET_FETCHING_MORE_RESOURCES,
  SET_USER_NOTIFICATIONS
} from './constants';


const initialState = {
  isFetching: false,
  isFetchingTypes: false,
  isFetchingResources: false,
  isFetchingMoreResources: false,
  isFetchingResourceById: false,
  isSubmittingShareForm: false,
  submitFormError: {},
  resourceTypes: [],
  productTypes: [],
  selectedProductType: null,
  selectedResourceType: null,
  resources: [],
  resourceViewing: {},
  shareFormStep: 1,
  isShareMode: false,
  resourceViewingError: {},
  resourcesError: {},
  userNotifications: {}
};

const reducer = function libraryReducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_LIBRARY_RESOURCE_TYPES:
      return {
        ...state,
        isFetchingTypes: true
      };
    case REQUEST_LIBRARY_RESOURCES:
      return {
        ...state,
        isFetchingResources: true,
        resourcesError: {}
      };
    case FETCHING_DATA_DONE:
      return {
        ...state,
        isFetching: false,
        isFetchingResources: false,
        isFetchingTypes: false
      }
    case REQUEST_LIBRARY_RESOURCE_BY_ID:
      return {
        ...state,
        resourceViewing: {},
        resourceViewingError: {},
        isFetchingResourceById: true
      };
    case FETCHING_LIBRARY_DATA:
      return {
        ...state,
        isFetching: action.data
      };
    case SET_LIBRARY_RESOURCE_TYPES:
      return {
        ...state,
        isFetchingTypes: false,
        resourceTypes: action.data,
      };
    case SET_LIBRARY_PRODUCT_TYPES:
      return {
        ...state,
        productTypes: action.data
      };
    case SET_LIBRARY_RESOURCE_VIEWING:
      return {
        ...state,
        isFetchingResourceById: false,
        resourceViewing: action.data,
      };
    case SET_LIBRARY_SELECTED_PRODUCT_TYPE:
      return {
        ...state,
        selectedProductType: action.data
      }
    case SET_LIBRARY_SELECTED_RESOURCE_TYPE:
      return {
        ...state,
        selectedResourceType: action.data
      }
    case ERROR_LIBRARY_RESOURCE_VIEWING:
      return {
        ...state,
        isFetchingResourceById: false,
        resourceViewingError: action.error
      };
    case SET_LIBRARY_RESOURCES:
      return {
        ...state,
        isFetchingResources: false,
        resources: action.data,
        resourcesError: {}
      };
    case ERROR_LIBRARY_RESOURCES:
      return {
        ...state,
        isFetchingResources: false,
        resourcesError: action.error
      };
    case SUBMITTING_SHARE_FORM:
      return {
        ...state,
        isSubmittingShareForm: true,
        submitFormError: {}
      }
    case SHARE_FORM_SUBMIT_SUCCESS:
      return {
        ...state,
        isSubmittingShareForm: false,
        submitFormError: {}
      }
    case SHARE_FORM_SUBMIT_ERROR:
      return {
        ...state,
        isSubmittingShareForm: false,
        submitFormError: action.data
      }
    case SET_LIBRARY_SHARE_FORM_STEP:
      return {
        ...state,
        shareFormStep: action.data
      }
    case SET_LIBRARY_IS_SHARE_MODE:
      return {
        ...state,
        isShareMode: action.data
      }
    case SET_FETCHING_MORE_RESOURCES:
      return {
        ...state,
        isFetchingMoreResources: action.data
      }
    case SET_USER_NOTIFICATIONS: {
      const notifications = {
        resources: !action.data.resources ? {} : action.data.resources,
        posts: !action.data.posts ? [] : action.data.posts
      }
      return {
        ...state,
        userNotifications: notifications
      }
    }
    default:
      return state;
  }
};

export default reducer;