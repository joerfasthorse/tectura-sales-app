import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom';
// import PageTransition from 'react-router-page-transition';

import App from '../App';
import Public from '../Public';
import AuthorizedRoute from './authorized-route';
import LoginRoute from './login-route';
import NotFound from '../components/404';
import Home from '../Public/home';

const Router = ({ location }) => (
  <BrowserRouter >
    {/* <PageTransition timeout={1000}> */}
      <Switch location={location}>
        <Route exact path="/" component={Home} />
        <LoginRoute path="/app-login" component={Public} />
        <AuthorizedRoute path="/app" component={App} />
        <Route exact path="/404" component={NotFound} />
        <Route component={NotFound} />
      </Switch>
    {/* </PageTransition> */}
  </BrowserRouter >
);

export default Router;
