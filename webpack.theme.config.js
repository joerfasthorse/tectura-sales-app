const ExtractTextPlugin = require('extract-text-webpack-plugin');
module.exports = {
    cache: true,
    entry: './src/javascript/index.jsx',
    output: {
        path: `${__dirname}/build`,
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        }, {
            test: /\.jsx$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        }, {
            test: /\.less$/,
            include: /styles/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'css-loader',
                    options: { url: false }
                }, {
                    loader: 'less-loader'
                }],
            }),
        }
        ]
    },
    plugins: [
        new ExtractTextPlugin('style.css')
    ],
    devServer: {
        index: 'index.html',
        historyApiFallback: true
    }
};
