import _ from 'lodash';
import { getFormSyncErrors } from 'redux-form';
import { LOGIN_FORM } from './constants';
export const isFetching = state => _.get(state, ['login', 'isFetching'], false);
export const isAuthenticated = state => _.get(state, ['login', 'isAuthenticated'], false);
export const getCurrentUser = state => _.get(state, ['login', 'user'], {});
export const getSyncErrors = state => getFormSyncErrors(LOGIN_FORM)(state);
export const hasSyncErrors = state => _.isEmpty(getFormSyncErrors(LOGIN_FORM)(state));
