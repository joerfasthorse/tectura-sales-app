import FetchApi from '../../helpers/fetch-helper';
const loginUrl = '/wp-json/jwt-auth/v1/token';
const validateTokenUrl = '/wp-json/jwt-auth/v1/token/validate';


export const loginUser = async (creds) => {
  try{
    const resp =  await FetchApi({
      url: loginUrl,
      method: 'POST',
      body: creds
    })
    
    return resp;
  } catch(err){
    return err;
  }
}

export const validateToken = async () => {
  try{
    const resp = await FetchApi({
      url: validateTokenUrl,
      method: 'POST'
    })
    return resp && resp.code === 'jwt_auth_valid_token' ? true : false;
  } catch(err){
    throw new Error(err)
  }
}