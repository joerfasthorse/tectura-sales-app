
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm, Field, getFormSubmitErrors } from 'redux-form';
import { requestLogin } from './actions';
import { LOGIN_FORM } from './constants';
import { renderInput } from './../../helpers/form-helpers';
import { isFetching, hasSyncErrors } from './selectors';
import validate from './validate';
const LoginForm = props => {

  const { error, hasError, handleSubmit, requestLogin } = props
  return (
    <div className="body login">
      <div className="main">
        <div className="content">
          <h1 className="logo title"><img src="wp-content/themes/tectura-react/images/tectura_logo.svg" alt="Tectura" /></h1>
          <h2 className="subtitle">Welcome</h2>
          <form className="widget-form" onSubmit={handleSubmit(fields => requestLogin({ creds: fields }))}>
            <p>Please enter your login credentials below:</p>
            <Field
              name="username"
              type="text"
              id="username"
              className="email icon-email"
              label="Enter your username"
              placeholder="Enter your username"
              component={renderInput}
            />
            <Field
              name="password"
              type="password"
              id="password"
              className="password icon-password"
              label="Password"
              placeholder="Password"
              component={renderInput}
            />
            {
              error && <div className="error-message" dangerouslySetInnerHTML={{ __html: error.message }}></div>
            }
            <p><a href="http://connect.tecturadesigns.com/wp-login.php?action=lostpassword">I forgot my password</a></p>
            <button action="submit" disabled={!hasError}>LOGIN</button>
          </form>
        </div>
      </div>
    </div>
  );
};


LoginForm.propTypes = {
  requestLogin: PropTypes.func
};

const mapStateToProps = state => ({
  isFetching: isFetching(state),
  error: getFormSubmitErrors(LOGIN_FORM)(state),
  hasError: hasSyncErrors(state)
});

const connected = connect(mapStateToProps, { requestLogin })(LoginForm);

const formed = reduxForm({
  form: LOGIN_FORM,
  validate
})(connected);

export default formed;