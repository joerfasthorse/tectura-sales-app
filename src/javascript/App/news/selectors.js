import _ from 'lodash';

export const getPosts = state => _.get(state, ['news', 'posts'], []);
export const getPostById = (state, id) => getPosts(state).find(post => post.id === parseInt(id));