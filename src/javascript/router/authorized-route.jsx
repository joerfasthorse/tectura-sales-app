import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { checkAuthentication} from '../Public/login/actions'
import { isAuthenticated, isFetching } from '../Public/login/selectors';
import {
  Route,
  Redirect
} from 'react-router-dom';

import LoadingComponent from '../components/loading'
class AuthorizedRoute extends React.Component{
  componentWillMount(){
    this.props.checkAuthentication();
  }
  render(){
    const { component: Component, isFetching, isAuthenticated, ...rest } = this.props;
    return (
      <Route {...rest} render={props => {
        if (isFetching) return <LoadingComponent/>;
        return isAuthenticated
          ? <Component {...props} />
          : <Redirect to="/app-login" />;
      }} />
    );
  }
}

AuthorizedRoute.propTypes = {
  isFetching: PropTypes.bool,
  isAuthenticated: PropTypes.bool
};
const mapStateToProps = state => {
  return {
    isFetching: isFetching(state),
    isAuthenticated: isAuthenticated(state)
  };
};
const mapDispatchToProps = dispatch => {
  return {
    checkAuthentication: () => dispatch(checkAuthentication()),
  };
};
export default connect(mapStateToProps,mapDispatchToProps)(AuthorizedRoute);
