import {
  REQUEST_LIBRARY_RESOURCE_TYPES,
  REQUEST_LIBRARY_RESOURCES,
  REQUEST_SUBMIT_SHARE_FORM,
  SET_LIBRARY_SHARE_FORM_STEP,
  SET_LIBRARY_IS_SHARE_MODE,
  SET_LIBRARY_RESOURCE_TYPE_PAGE,
  SET_LIBRARY_RESOURCE_PAGE,
  REQUEST_LIBRARY_PRODUCT_TYPES,
  REQUEST_LIBRARY_RESOURCE_BY_ID,
  SET_LIBRARY_SELECTED_PRODUCT_TYPE,
  REQUEST_USER_NOTIFICATIONS
} from './constants';

// In order to perform an action of type LOGIN_REQUESTING
// we need an email and password

export const requestResourceTypes = () => ({type: REQUEST_LIBRARY_RESOURCE_TYPES});
export const requestProductTypes = () => ({type: REQUEST_LIBRARY_PRODUCT_TYPES});
export const requestResources = payload => ({type: REQUEST_LIBRARY_RESOURCES, payload});
export const requestUserNotifications = () => ({type: REQUEST_USER_NOTIFICATIONS});
export const requestResourceById = id => ({type: REQUEST_LIBRARY_RESOURCE_BY_ID, id});
export const setShareFormStep = data => ({type: SET_LIBRARY_SHARE_FORM_STEP, data});
export const setIsShareMode = data => ({type: SET_LIBRARY_IS_SHARE_MODE, data});
export const submitShareForm = () => ({type: REQUEST_SUBMIT_SHARE_FORM});
export const setReourcePage = data => ({type: SET_LIBRARY_RESOURCE_PAGE, data});
export const setReourceTypePage = data => ({type: SET_LIBRARY_RESOURCE_TYPE_PAGE, data});
export const setSelectedProductType = data => ({type: SET_LIBRARY_SELECTED_PRODUCT_TYPE, data});