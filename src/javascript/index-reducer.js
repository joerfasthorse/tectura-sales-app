import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'
import { reducer as form } from 'redux-form';
// import appMenu from "./app-menu/reducer";
import login from './Public/login/reducer';
import appMenu from './App/app-menu/reducer';
import request from './App/request/reducer';
import library from './App/library/reducer';
import news from './App/news/reducer';
const IndexReducer = combineReducers({
  // appMenu,
  form,
  login,
  appMenu,
  request,
  library,
  news,
  router: routerReducer
});

export default IndexReducer;