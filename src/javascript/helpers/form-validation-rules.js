export const email = value => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value);
export const zipcode = value => /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(value);