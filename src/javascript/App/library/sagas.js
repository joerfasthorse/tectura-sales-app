import { takeLatest, call, put, select } from 'redux-saga/effects';
import {
  getResourceTypes,
  getShareFormData,
  getCurrentUser,
  getNewResources
} from './selectors';
import {
  fetchResourceTypes,
  fetchResources,
  submitShareForm,
  fetchProductTypes,
  fetchResourceById,
  fetchMoreResource,
  fetchUserNotifications
} from './api';
// Our login constants
import {
  REQUEST_LIBRARY_RESOURCE_TYPES,
  SET_LIBRARY_RESOURCE_TYPES,
  REQUEST_LIBRARY_RESOURCES,
  SET_LIBRARY_RESOURCES,
  REQUEST_LIBRARY_RESOURCE_BY_ID,
  SET_LIBRARY_RESOURCE_VIEWING,
  REQUEST_SUBMIT_SHARE_FORM,
  SUBMITTING_SHARE_FORM,
  SHARE_FORM_SUBMIT_SUCCESS,
  SHARE_FORM_SUBMIT_ERROR,
  REQUEST_LIBRARY_PRODUCT_TYPES,
  SET_LIBRARY_PRODUCT_TYPES,
  ERROR_LIBRARY_RESOURCE_VIEWING,
  ERROR_LIBRARY_RESOURCES,
  SET_LIBRARY_SELECTED_RESOURCE_TYPE,
  FETCHING_DATA_DONE,
  SET_FETCHING_MORE_RESOURCES,
  REQUEST_USER_NOTIFICATIONS,
  SET_USER_NOTIFICATIONS
} from './constants';

import { setIsShareMode, setShareFormStep } from './actions';
function* requestResourceTypeFetchSaga() {
  try {
    const state = yield select();
    if (getResourceTypes(state).length === 0) {
      const data = yield call(fetchResourceTypes);
      yield put({ type: SET_LIBRARY_RESOURCE_TYPES, data });
    }
  } catch (err) {
    // error? send it to redux
  }
  // return the token for health and wealth
}
function* requestResourcesFetchSaga({ payload: { resourceType } }) {
  try {
    const state = yield select();
    const selectType = state.library.selectedResourceType;
    if (selectType !== resourceType) {
      const resp = yield call(fetchResources, resourceType);
      yield put({ type: SET_LIBRARY_RESOURCES, data: resp.data });
      yield put({ type: SET_LIBRARY_SELECTED_RESOURCE_TYPE, data: resourceType })
      if (resp.data.length < resp.pagination.total) {
        yield put({ type: SET_FETCHING_MORE_RESOURCES, data: true })
        const moreResp = yield call(fetchMoreResource, {
          type: resourceType,
          totalPage: resp.pagination.totalPage
        });
        yield put({ type: SET_LIBRARY_RESOURCES, data: resp.data.concat(moreResp) });
        yield put({ type: SET_FETCHING_MORE_RESOURCES, data: false })
      }
    } else {
      yield put({ type: FETCHING_DATA_DONE });
    }

  } catch (error) {
    // error? send it to redux
    yield put({ type: ERROR_LIBRARY_RESOURCES, error });
  }
  // return the token for health and wealth
}
function* requestResourceByIdFetchSaga({ id }) {
  try {
    const state = yield select();
    const user = getCurrentUser(state);
    const data = yield call(fetchResourceById, id, user.user_id);
    const newResources = getNewResources(state);
    if(newResources.hasOwnProperty(id)){
      delete newResources[id];
    }
    yield put({ type: SET_LIBRARY_RESOURCE_VIEWING, data });
  } catch (error) {
    yield put({ type: ERROR_LIBRARY_RESOURCE_VIEWING, error });
    // error? send it to redux
  }
  // return the token for health and wealth
}

function delay(ms) {
  return new Promise(resolve => setTimeout(() => resolve(true), ms))
}
function* submitShareFormSaga() {
  try {
    yield put({ type: SUBMITTING_SHARE_FORM });
    const shareData = yield select(getShareFormData);
    const resp = yield call(submitShareForm, shareData);
    if (resp) {
      yield put({ type: SHARE_FORM_SUBMIT_SUCCESS });
      yield put(setShareFormStep(3))
      yield call(delay, 3000),
        yield put(setIsShareMode(false))
    } else {
      yield put({ type: SHARE_FORM_SUBMIT_ERROR, data: resp });
    }
  } catch (error) {
    yield put({ type: SHARE_FORM_SUBMIT_ERROR, data: error });

  }
}

function* requestProductTypesFetchSaga() {
  try {
    const data = yield call(fetchProductTypes);
    yield put({ type: SET_LIBRARY_PRODUCT_TYPES, data });
  } catch (error) {
    // error? send it to redux
  }
  // return the token for health and wealth
}

function* requestUserNotifications() {
  try {
    const state = yield select();
    const user = getCurrentUser(state);
    const notifications = yield call(fetchUserNotifications, user.user_id);
    if(notifications){
      yield put({ type: SET_USER_NOTIFICATIONS, data : notifications});
    }
  } catch (error) {
     // error? send it to redux
  }
}
// eslint-disable-next-line
function* mySaga() {
  yield takeLatest(REQUEST_LIBRARY_RESOURCE_TYPES, requestResourceTypeFetchSaga);
  yield takeLatest(REQUEST_LIBRARY_RESOURCES, requestResourcesFetchSaga);
  yield takeLatest(REQUEST_LIBRARY_RESOURCE_BY_ID, requestResourceByIdFetchSaga);
  yield takeLatest(REQUEST_SUBMIT_SHARE_FORM, submitShareFormSaga);
  yield takeLatest(REQUEST_LIBRARY_PRODUCT_TYPES, requestProductTypesFetchSaga);
  yield takeLatest(REQUEST_USER_NOTIFICATIONS, requestUserNotifications);

}

export default mySaga;