import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
const AccountPage = props => {
    return (
        <div className="page-transition">
            <div className="body">
                <h1>Settings</h1>
            </div>
        </div>
    );
};

AccountPage.propTypes = {
};
const mapStateToProps = state => {
    return {
    };
};

const mapDispatchToProps = dispatch => {
    return {
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(AccountPage);
