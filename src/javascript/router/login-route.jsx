import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Route,
  Redirect
} from 'react-router-dom';
import { isAuthenticated, isFetching, getCurrentUser } from '../Public/login/selectors';

const LoginRoute = ({ component: Component, isAuthenticated, ...rest }) => {
  return (
    <Route {...rest} render={props => {
      return isAuthenticated
        ? <Redirect to="/app" />
        : <Component {...props} />;
    }} />
  );
};

LoginRoute.propTypes = {
  isFetching: PropTypes.bool,
  isAuthenticated: PropTypes.bool
};
const mapStateToProps = state => {
  return {
    isFetching: isFetching(state),
    isAuthenticated: isAuthenticated(state),
    user: getCurrentUser(state)
  };
};
export default connect(mapStateToProps)(LoginRoute);
