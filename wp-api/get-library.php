<?php
// add_action( 'rest_api_init', 'tectura_get_attachments' );
// function tectura_get_attachments() {
// 	register_rest_route( 'tectura', '/get-attachments', array(
//         'methods' => 'GET',
//         'callback' => 'tectura_get_attachments_callback',
//     ));
// }
// function tectura_get_attachments_callback( $request_data ) {
//     $data = $request_data->get_params();
//     $attachmentIds = $data['attachments'];
//     $attachments = array();
    
//     foreach($attachmentIds as &$id){
//         $attachment = wp_get_attachment_image_url($id, 'medium');
//         array_push($attachments, array('image' => $attachment, 'id' => $id));
//     }
//     return $attachments;
// }

// Hook in to add image url to resources video or file.

add_filter( 'rest_post_dispatch', function (
	\WP_HTTP_Response $response,
	\WP_REST_Server $server,
	\WP_REST_Request $request
) {
    $type = $request->get_params()['type'];
    if($type === 'tectura_resource'){
        $resources = $response->get_data();
        foreach($resources as &$resource){
            // if(!empty($resource['file'])){
            //     $resource['file'] = getImageUrl($resource['file']);
            // } else if(!empty($resource['video'])){
            //     var_error_log(getImageUrl($resource['video']));
            //     $resource['video'] = getImageUrl($resource['video']);
            // }
            if(!empty($resource['thumbnail'])){
                $resource['thumbnail']['image_url'] = wp_get_attachment_image_url($resource['thumbnail']['ID'], 'medium_large');
            }
            
            // if(!empty($resource['gallery'])){
            //     $resource['gallery'] = getGalleryImage($resource['gallery']);
            // }
        }
        $response->set_data($resources);
    } else if($type === 'tectura_resource_single'){
        $resource = $response->get_data();
        if(!empty($resource['file'])){
            $resource['file'] = getImageUrl($resource['file']);
        } else if(!empty($resource['video'])){
            $resource['video'] = getImageUrl($resource['video']);
        }
        add_image_size( 'category-thumb', 500, 9999 );
        if(!empty($resource['thumbnail'])){
            $resource['thumbnail']['image_url'] = wp_get_attachment_image_url($resource['thumbnail']['ID'], 'medium_large');
        }
        
        if(!empty($resource['gallery'])){
            $resource['gallery'] = getGalleryImage($resource['gallery']);
        }

        // remove resoure from user meta because they have viewed it.
        $userId = $request->get_params()['user_id'];
        $newResources = get_user_meta( $userId, TECTURA_NEW_RESOURCES, false );
        if(array_key_exists($resource['id'], $newResources)){
            error_log( print_r($newResources, TRUE));
            $updated = $newResources;
            unset($updated[$resource['id']]);
            update_user_meta( $userId, TECTURA_NEW_RESOURCES, $updated, $newResources );
        }
        $response->set_data($resource);
    }
    return $response;
}, 0, 3 );
function getGalleryImage($gallery){
    foreach($gallery as &$image){
        $image['image_url']= wp_get_attachment_image_url($image['ID'], 'medium');
    }
    return $gallery;
}
function getImageUrl($assets){
    foreach($assets as &$asset){
        $asset['image_url']= wp_get_attachment_image_url($asset['image'], 'medium_large');
    }
    return $assets;
}