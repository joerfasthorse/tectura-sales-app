import React from 'react';
import { connect } from 'react-redux';
const NotFound = () => {
  return (
    <div className="body">
      <h1>Page Not Found</h1>
    </div>
  );
};

export default connect()(NotFound);
