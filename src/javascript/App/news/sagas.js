import { takeLatest, call, put, select } from 'redux-saga/effects';
import { fetchPosts, updateViewedPost } from './api';
import { getUserNotifications, getCurrentUser } from '../library/selectors';
// Our login constants
import {
  FETCHING_POSTS_DATA,
  REQUEST_POSTS_DATA,
  SET_POSTS_DATA,
  FETCH_POSTS_SUCCESSFUL,
  UPDATE_POST_NOTIFICTION
} from './constants';

import { SET_USER_NOTIFICATIONS } from '../library/constants';

// eslint-disable-next-line
function* requestFetchSaga({ }) {
  try {
    yield put({ type: FETCHING_POSTS_DATA });
    const data = yield call(fetchPosts);
    yield put({ type: FETCH_POSTS_SUCCESSFUL });
    yield put({ type: SET_POSTS_DATA, data });
  } catch (error) {
    // error? send it to redux
  }
  // return the token for health and wealth
}
// eslint-disable-next-line
function* updatePostNotificationSaga({userId, postId }) {
  try {
    const state = yield select();
    const user = getCurrentUser(state);

    const data = yield call(updateViewedPost, user.user_id, postId);
    if(data){
      const state = yield select();
      const notifications = {
        ...getUserNotifications(state),
        posts: data
      }
      yield put({type: SET_USER_NOTIFICATIONS, data : notifications});
    }
  } catch (error) {
    // error? send it to redux
  }
  // return the token for health and wealth
}

// eslint-disable-next-line
function* mySaga() {
  yield takeLatest(REQUEST_POSTS_DATA, requestFetchSaga);
  yield takeLatest(UPDATE_POST_NOTIFICTION, updatePostNotificationSaga)
}

export default mySaga;