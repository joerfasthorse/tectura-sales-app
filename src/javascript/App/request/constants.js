
export const REQUEST_FORM_NAMESPACE = 'requestFormStepOne';

export const REQUEST_RESOURCE_TYPES = 'request/request-resource-types';
export const REQUEST_RESOURCE = 'request/request-resouce';
export const FETCHING_RESOURCE_TYPES = 'request/fetching-resource-types';
export const FETCHING_RESOURCES = 'request/fetching-resource';
export const RESOURCE_TYPES_SUCCESS = 'request/resource-type-success';
export const RESOURCES_SUCCESS = 'request/resources-success';
export const REQUEST_SUBMIT_FORM = 'request/request-submit-form';
export const SUBMITTING_FORM= 'request/submitting-form';
export const FORM_SUBMIT_SUCCESS = 'request/form-submit-success';
export const SET_REQUEST_FORM_STEP ='request/set-request-form-step';
export const SUBMIT_REQUEST_FORM_FOR_VALIDATE ='request/submit-request-form-for-validate';