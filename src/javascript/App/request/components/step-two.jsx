import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, FieldArray, change, getFormValues, submit} from 'redux-form';
import { getSelectedResources, getType, getRecipients, getSyncErrors } from '../selectors';
import RecipientInfoView from './recipient-info-view';
import QuanityInput from './quanity-input';
import { REQUEST_FORM_NAMESPACE } from '../constants';
import RecipientInfoEdit from './recipient-info-edit';

class StepTwo extends React.Component{
  constructor(props) {
    super(props);
    this.state = { isEditRecipient: false };
    this.handleEdit = this.handleEdit.bind(this);
  }
  handleEdit(event){
    const isError = Object.keys(this.props.syncError).length !== 0;
    const { isEditRecipient } = this.state;
    if(!isEditRecipient || !isError){
      event.preventDefault();
      this.setState({isEditRecipient: !isEditRecipient});
    }
    
  }
  render(){
    const { recipients, changeField, resources } = this.props;
    const { isEditRecipient } = this.state;
    const removeResourceHanlder = (index) => {
      const isSelected = `resources[${index}].isSelected`;
      const quanity = `resources[${index}].quanity`;
      changeField(isSelected, false);
      changeField(quanity, 1);
    }
    return (
  
      <div className="main steptwo">
        <h3>You've selected the following literature items:</h3>
        <FieldArray name="resources" removeHandler={removeResourceHanlder} resources={resources} component={QuanityInput} />
  
        <h3>Send file to:</h3>
  
        {
          isEditRecipient ? 
          <FieldArray name="recipients" component={RecipientInfoEdit} />:
          <RecipientInfoView recipients={recipients} />
        }
        <button onClick={this.handleEdit}>{isEditRecipient ? 'done' : 'edit'}</button>
        <hr />
        <h3>Comments:</h3>
        <div>
          <Field
            name="comments"
            className="comments"
            type="textarea"
            component="textarea"
            label="Comments"
            placeholder="" />
        </div>
      </div>
    );
  }
}

StepTwo.propTypes = {
  typeName: PropTypes.string,
  selectedTypes: PropTypes.array
};
const mapStateToProps = state => {
  return {
    typeName: getType(state),
    resources: getSelectedResources(state),
    recipients: getRecipients(state),
    syncError: getSyncErrors(state),
    formData: getFormValues(REQUEST_FORM_NAMESPACE)(state)

  };
};
const mapDispatchToProps = dispatch => {
  return {
    changeField: (field, value) => dispatch(change(REQUEST_FORM_NAMESPACE, field, value)),
    submitForm: () => dispatch(submit(REQUEST_FORM_NAMESPACE))
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(StepTwo);
