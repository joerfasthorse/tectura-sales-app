import _ from 'lodash';
import { LIBRARY_SHARE_FORM } from './constants';
import { getFormSyncErrors, formValueSelector } from 'redux-form';

const selector = formValueSelector(LIBRARY_SHARE_FORM);
const mapAssets = resource => {
  let assets = null;
  if (resource.file && resource.file.length > 0) {
    assets = resource.file && resource.file.map(i => ({
      id: i.id,
      title: i.file_title,
      link: i.link,
      image: i.image_url ? i.image_url : false
    }))
  }
  if (resource.video && resource.video.length > 0) {
    assets = resource.video && resource.video.map(i => ({
      id: i.id,
      title: i.video_title,
      videoId: i.video_id,
      image: i.image_url ? i.image_url : false
    }))
  }
  if (resource.gallery && resource.gallery.length > 0) {
    assets = resource.gallery && resource.gallery.map(i => ({
      id: i.ID,
      link: i.guid,
      title: i.post_name,
      fullImage: i.guid ? i.guid : false,
      image: i.image_url
    }))
  }
  return assets ? assets : [];
}
const getSelectedAssets = state => selector(state, 'assets').filter(i => i.isSelected);
export const getResourceTypes = state => _.get(state, ['library', 'resourceTypes'], []);
export const getProductTypes = state => _.get(state, ['library', 'productTypes'], []);
export const getResources = state => _.get(state, ['library', 'resources'], []);
export const getFilterResources = state => {
  const productType = state.library.selectedProductType || 'all';
  const resources = getResources(state);
  if (productType === 'all') {
    return resources;
  } else {
    return resources.filter(r => {
      return r.product_type.includes(parseInt(productType))
    });
  }

}
export const getResourceViwing = state => {
  const resource = _.get(state, ['library', 'resourceViewing'], {});
  return !_.isEmpty(resource) ? {
    ...resource,
    asset_type: !resource.asset_type ? 'gallery' : resource.asset_type[0],
    assets: mapAssets(resource)
  } : {};
}
export const getSyncErrors = state => getFormSyncErrors(LIBRARY_SHARE_FORM)(state);
export const hasSyncErrors = state => _.isEmpty(getFormSyncErrors(LIBRARY_SHARE_FORM)(state));
export const getShareFormData = state => ({
  assets: getSelectedAssets(state).map(i => ({
    id: i.id,
    link: i.link,
    title: i.title,
    videoId: i.videoId || null
  })),
  recipients: selector(state, 'recipients').map(i => i.email),
  comments: selector(state, 'comments'),
  resourceName: selector(state, 'resourceName'),
  dropboxFolder: selector(state, 'dropboxFolder'),
})
export const getBreadcrumb = (state, type, resourceId = null) => {
  const resourceType = type ? getResourceTypes(state).find(i => i.slug === type) : null;
  const resource = resourceId ? getResourceViwing(state) : null;
  if (!resourceType) return [];
  return [resourceType.name, !_.isEmpty(resource) ? resource.title.rendered : null]

}

export const getCurrentUser = state => _.get(state, ['login', 'user'], {});
export const getUserNotifications = state => _.get(state, ['library', 'userNotifications'], {});
export const getNewPosts = state => _.get(getUserNotifications(state), ['posts'], []);
export const getNewResources = state => _.get(getUserNotifications(state), ['resources'], {});