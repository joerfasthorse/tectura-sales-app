import FetchApi from '../../helpers/fetch-helper';
const getResourceTypeUrl = '/wp-json/wp/v2/resource_type?type=tectura_resource';
export  const fetchResourceTypes = async () => {
  try{
    const data =  await FetchApi({
      url: getResourceTypeUrl
    })
    return data && data.filter(type => !type.is_request || type.is_request !== '1');
  } catch(err){
    throw err;
  }
}
const getResourcesUrl = (type, page) => `/wp-json/wp/v2/resources?type=tectura_resource&per_page=50&page=${page ? page : '1'}&filter[resource_type]=${type}`;
export const fetchResources = async (resourceType) => {
  try{
    const resp =  await FetchApi({
      url: getResourcesUrl(resourceType),
      returnHeaders: true,
    })
    const pagination = {
      total: resp.headers.get('X-WP-Total'),
      totalPage: resp.headers.get('X-WP-TotalPages')
    }
    return await {
      data : resp.data,
      pagination: pagination
    };
  } catch(err){
    throw err;
  }
}

export const fetchMoreResource = async ({type, totalPage}) => {
  const promises = [];
  for(let page = 2; page <= totalPage; page += 1){
    promises.push(FetchApi({
      url: getResourcesUrl(type, page)
    }))
  }
  const resp = await Promise.all(promises);
  return resp.reduce((all, i ) => {
    return all.concat(i)
  },[])
}
const getResourceByIdUrl = (id, userId) => `/wp-json/wp/v2/resources/${id}?type=tectura_resource_single&user_id=${userId}`

export const fetchResourceById = async (id, userId) => {
  try{
    const resp =  await FetchApi({
      url: getResourceByIdUrl(id, userId)
    })
    return await resp;
  } catch(err){
    throw err;
  }
}

const shareFormUrl = '/wp-json/tectura/library/submitshare';
export  const submitShareForm = async (request) => {
  try{
    const data =  await FetchApi({
      url: shareFormUrl,
      method: 'POST',
      body: request
    })
    return data;
  } catch(err){
    throw err;
  }
}

const getProductTypesUrl = '/wp-json/wp/v2/product_type';
export const fetchProductTypes = async () => {
  try{
    const data =  await FetchApi({
      url: getProductTypesUrl
    })
    return data;
  } catch(err){
    throw err;
  }
}

const getUserNewResources = id => `/wp-json/tectura/user/new-resources?id=${id}`;
export const fetchUserNewResources = async (id) => {
  try{
    const data = await FetchApi({
      url: getUserNewResources(id)
    })
    return data;
  } catch(err){
    throw err
  }
}

const getUserNotifications = id => `/wp-json/tectura/user/new-notifications?id=${id}`;
export const fetchUserNotifications = async (id) => {
  try{
    const data = await FetchApi({
      url: getUserNotifications(id)
    })
    return data;
  } catch(err){
    throw err
  }
}
