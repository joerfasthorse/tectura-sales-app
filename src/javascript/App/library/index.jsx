import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getResourceTypes, getNewResources, getCurrentUser } from './selectors';
import { requestResourceTypes } from './actions';
import { makeUrl } from './../../helpers/helpers'

const getCountOfResourceInType = (resources, type) => Object.keys(resources).reduce((count, id) =>{
  const resource = resources[id];
  if(resource.resource_type.includes(type)){
    count =+ 1;
  }
  return count;
},0)
class LibraryPage extends React.Component {
  componentDidMount() {
    this.props.requestResourceTypes();
  }
  render() {
    const { resourceTypes, history, match, newResources } = this.props;
    const goToPage = category => history.push(`${makeUrl(match.url)}${category}`);
    return (
      <div className="page-transition">
        <div className="body library">
          <div className="main flex">
            {
              resourceTypes && resourceTypes.map(type => {
                const thumbnail = type.thumbnail ? type.thumbnail.image_url : '';
                const style = {
                  backgroundImage: `url(${thumbnail})`
                }
                const newCount = getCountOfResourceInType(newResources ,type.id);
                return (
                  <div key={type.id + type.slug}
                    style={style}
                    className={'flex-item ' + type.slug.toLowerCase()}
                    onClick={() => goToPage(type.slug)}>
                    {
                    newCount > 0 && <span className="badge">{newCount} new</span>
                    }
                    <h2 className="title">{type.name}</h2>
                  </div>
                );
              })
            }
          </div>
        </div>
      </div>
    );
  }
}
LibraryPage.propTypes = {
  categories: PropTypes.array,
  breadcrumbs: PropTypes.array,
  newResources: PropTypes.object
};
const mapStateToProps = state => {
  return {
    isFetching: state.isFetchingTypes,
    resourceTypes: getResourceTypes(state),
    breadcrumbs: state.library.breadcrumbs,
    newResources: getNewResources(state),
    user: getCurrentUser(state)
  };
};

export default connect(mapStateToProps, { requestResourceTypes })(LibraryPage);
