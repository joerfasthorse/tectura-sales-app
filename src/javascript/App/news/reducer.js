import {
  FETCHING_POSTS_DATA,
  FETCH_POSTS_SUCCESSFUL,
  SET_POSTS_DATA
} from './constants';

const initialState = {
  fetchingData: false,
  posts: []
};

const reducer = function requestReducer(state = initialState, action) {
  switch (action.type) {
    case FETCHING_POSTS_DATA:
      return {
        ...state,
        fetchingData: action.data
      };
    case FETCH_POSTS_SUCCESSFUL:
      return {
        ...state,
        fetchingData: false
      };
    case SET_POSTS_DATA:
      return {
        ...state,
        posts: action.data
      };
    default:
      return state;
  }
};

export default reducer;