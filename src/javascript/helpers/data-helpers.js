import moment from 'moment'

export const renderPostDate = date => date ? moment(date).format('MMMM DD, YYYY') : '';
