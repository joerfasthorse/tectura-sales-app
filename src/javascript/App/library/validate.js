import * as Validate from './../../helpers/form-validation-rules';

const validate = values => {
  const errors = {};
  if (!values.recipients || !values.recipients.length) {
    errors.recipients = { _error: 'At least one recipient must be entered' };
  } else {
    const recipientsArrayErrors = [];
    values.recipients.forEach((recipient, recipientIndex) => {
      const recipientErrors = {};
      if (recipient.email && !Validate.email(recipient.email)) {
        recipientErrors.email = 'Invalid Email';
        recipientsArrayErrors[recipientIndex] = recipientErrors;
      } if(!recipient.email){
        recipientErrors.email = 'Email is Empty';
        recipientsArrayErrors[recipientIndex] = recipientErrors;
      }
    });
    if (recipientsArrayErrors.length) {
      errors.recipients = recipientsArrayErrors;
    }
  }
  if (values.assets) {
    const hasSelected = values.assets.find(i => i.isSelected);
    if(!hasSelected){
      errors.assets = { _error: 'At least one asset must be selected' };
    }
  }
  return errors;
};

export default validate;
