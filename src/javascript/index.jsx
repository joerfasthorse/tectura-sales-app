import 'babel-polyfill';
import 'whatwg-fetch';
import Promise from 'promise-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
// import logger from 'redux-logger'
// Import the index reducer and sagas
import IndexReducer from './index-reducer';
import IndexSagas from './index-sagas';

import './../less/styles.less';
import Router from './router';
const sagaMiddleware = createSagaMiddleware();
/*eslint-disable */
const composeSetup = process.env.NODE_ENV !== 'production' && typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose
/*eslint-enable */
import {
    ConnectedRouter,
    routerMiddleware
} from 'react-router-redux'
import createHistory from 'history/createBrowserHistory';
const history = createHistory();
const reduxRouterMiddleware = routerMiddleware(history);

const store = createStore(
    IndexReducer,
    composeSetup(applyMiddleware(sagaMiddleware, reduxRouterMiddleware)), // allows redux devtools to watch sagas
);

// To add to window
if (!window.Promise) {
    window.Promise = Promise;
  }
// Begin our Index Saga
sagaMiddleware.run(IndexSagas);


const AppInit = () => (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Router />
        </ConnectedRouter>
    </Provider>
);

/* global document */
ReactDOM.render(<AppInit />, document.getElementById('tectura-app'));
