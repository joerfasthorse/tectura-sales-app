import {
  REQUEST_RESOURCE_TYPES,
  REQUEST_RESOURCE,
  REQUEST_SUBMIT_FORM,
  SET_REQUEST_FORM_STEP,
  SUBMIT_REQUEST_FORM_FOR_VALIDATE
} from './constants';

export const requestResouceTypes = () => ({ type: REQUEST_RESOURCE_TYPES });
export const requestResouces = resourceType => ({ type: REQUEST_RESOURCE, typeId : resourceType });
export const setRequestFormStep = data => ({type: SET_REQUEST_FORM_STEP, data});
export const requestSubmit = payload => ({ type: REQUEST_SUBMIT_FORM, payload });
export const sumbitFormForValidate = () => {
  return {type: SUBMIT_REQUEST_FORM_FOR_VALIDATE};
}