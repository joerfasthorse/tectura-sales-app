import React from 'react';

export const renderInput = ({ input, label, type, className, meta: { touched, error, warning } }) => (
  <div className={"input " + className }  >
    <input {...input}placeholder={label} type={type} />
    {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
  </div>
);

export const renderCheckbox = ({ input, label, meta: { touched, error, warning } }) => (
  <label ><span dangerouslySetInnerHTML={{ __html: label }}></span>
    <input {...input} placeholder={label} type="checkbox" />
    <span className="checkbox icon-check"></span>
    {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
  </label>
);

export const renderSelect = ({
  input,
  label,
  children,
  meta: { touched, error, warning }
  }) => (
    <div>
      <label>{label}</label>
      <div className="select">
        <select {...input} >
          {children}
        </select>
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  );
