<?php
add_filter('pods_api_post_save_pod_item_resources', 'my_post_save_resource_function', 10, 3);
function my_post_save_resource_function($pieces, $is_new_item, $id) {
	$users = get_users( array( 'fields' => array( 'ID' ) ) );
	$terms = wp_get_post_terms( $id, "resource_type" );
	$termIds = array_map(function($e){
		return $e->term_id;
	}
	,$terms);
	$newResource = array("resource_type"=>$termIds);
	foreach($users as $user_id) {
		$resources = get_user_meta( $user_id->ID, TECTURA_NEW_RESOURCES, true );
		// 		delete_user_meta($user_id->ID, META_KEY);
		if(empty($resources)){
			$array = array($id => $newResource);
			update_user_meta( $user_id->ID, TECTURA_NEW_RESOURCES, $array);
		}
		else{
			$newResources = $resources;
			$newResources[$id] = $newResource;
			update_user_meta( $user_id->ID, TECTURA_NEW_RESOURCES, $newResources, $resources );
		}
	}
	return $pieces;
}
function post_published_notification( $ID, $post ) {
	$users = get_users( array( 'fields' => array( 'ID' ) ) );
	foreach($users as $user_id) {
				$newPosts = get_user_meta( $user_id->ID, TECTURA_NEW_POSTS, true );
				// delete_user_meta( $user_id->ID, TECTURA_NEW_POSTS);
		if(empty($newPosts)){
			update_user_meta( $user_id->ID, TECTURA_NEW_POSTS, array($ID));
		}
		elseif(!in_array($ID, $newPosts)){
			$new = $newPosts;
			array_push($new, $ID);
			update_user_meta( $user_id->ID, TECTURA_NEW_POSTS, $new, $newPosts );
		}
	}
}
add_action( 'publish_post', 'post_published_notification', 10, 2 );

function tectura_get_user_new_notifications_callback($request_data){
	$data = $request_data->get_params();
	$resources = get_user_meta( $data['id'], TECTURA_NEW_RESOURCES, true );
	if(empty($resources)){
		$resources = null;
	}
	$newPosts = get_user_meta($data['id'], TECTURA_NEW_POSTS, true);
	if(empty($newPosts)){
		$newPosts = null;
	}
	$notifications = array('resources'=> $resources, 'posts' => $newPosts);
	return $notifications;
}

function tectura_get_user_new_notifications() {
	register_rest_route( 'tectura', '/user/new-notifications', array(
	        'methods' => 'GET',
	        'callback' => 'tectura_get_user_new_notifications_callback'
		));
}

add_action( 'rest_api_init', 'tectura_get_user_new_notifications' );

function tectura_viewed_post_callback($request_data){
	$data = $request_data->get_params();
	if($data['user_id'] && $data['post_id']){
        $newPosts = get_user_meta( $data['user_id'], TECTURA_NEW_POSTS, true );
        if(in_array($data['post_id'], $newPosts)){
            $updated = $newPosts;
            $index = array_search($data['post_id'], $updated);
            unset($updated[$index]);
            update_user_meta( $data['user_id'], TECTURA_NEW_POSTS, $updated, $newPosts );
            return $updated;
        }
    }
}

function tectura_viewed_post() {
	register_rest_route( 'tectura', '/user/viewed-post', array(
	        'methods' => 'GET',
	        'callback' => 'tectura_viewed_post_callback'
		));
}

add_action( 'rest_api_init', 'tectura_viewed_post' );


function clearUserNotifications(){
	$users = get_users( array( 'fields' => array( 'ID' ) ) );
	foreach($users as $user_id) {
		delete_user_meta( $user_id->ID, TECTURA_NEW_POSTS);
		delete_user_meta( $user_id->ID, TECTURA_NEW_RESOURCES);
	}
}


// clearUserNotifications();
