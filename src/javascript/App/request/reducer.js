import {
  FETCHING_RESOURCE_TYPES,
  FETCHING_RESOURCES,
  RESOURCE_TYPES_SUCCESS,
  RESOURCES_SUCCESS,
  SUBMITTING_FORM,
  FORM_SUBMIT_SUCCESS,
  SET_REQUEST_FORM_STEP
} from './constants';

const initialState = {
  fetchingResourceTypes: false,
  fetchingResources: false,
  isSubmittingRequest: false,
  requestFormStep: 1,
  options: [
    { name: 'New Customer', value: 'New Customer' },
    { name: 'Existing Client', value: 'Existing Client' },
    { name: 'Myself', value: 'Myself' }
  ],
  resourceTypes: [],
  resources: []
};

const reducer = function requestReducer(state = initialState, action) {
  switch (action.type) {
    case FETCHING_RESOURCE_TYPES:
      return {
        ...state,
        fetchingResourceTypes: true
      };
    case FETCHING_RESOURCES:
      return {
        ...state,
        resources: [],
        fetchingResources: false
      };
    case RESOURCE_TYPES_SUCCESS:
      return {
        ...state,
        resourceTypes: action.data,
        fetchingResourceTypes: false
      };
    case RESOURCES_SUCCESS:
      return {
        ...state,
        resources: action.data,
        fetchingResources: false
      };
    case SUBMITTING_FORM:
      return {
        ...state,
        isSubmittingRequest: true
      };
    case FORM_SUBMIT_SUCCESS:
      return {
        ...state,
        isSubmittingRequest: false
      };
    case SET_REQUEST_FORM_STEP:
      return {
        ...state,
        requestFormStep: action.data
      }
    default:
      return state;
  }
};

export default reducer;