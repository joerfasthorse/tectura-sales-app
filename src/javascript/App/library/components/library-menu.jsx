import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getResourceTypes } from './../selectors';
import { NavLink } from 'react-router-dom';
import { requestResourceTypes } from './../actions';

class LibraryMenu extends React.Component {
  componentDidMount() {
    this.props.requestResourceTypes()
  }
  render() {
    const { resourseTypes, history } = this.props;
    return (
      <div className="main-navigation">
        <ul>
          <li key="Home">
            <NavLink to="/app" className="icon-home" activeClassName="active">Home</NavLink>
          </li>
          {
            resourseTypes && resourseTypes.map(resource => {
              return (
                <li key={resource.id}>
                  <NavLink to={`/app/library/${resource.slug}`} className={'icon-' + resource.name.replace(/\s+/g, '-').toLowerCase()} activeClassName="active">
                    {resource.name}
                  </NavLink>
                </li>
              );
            })
          }
        </ul>
        {/* Khoa can we move this out into it's own div? compiler won't let me */}
        <div className="secondary-navigation">
          <button onClick={() => history.goBack()} className="back icon-arrow-left"></button>
        </div>
      </div>

    );
  }
}

LibraryMenu.propTypes = {
  resourseType: PropTypes.array
};
const mapStateToProps = state => {
  return {
    resourseTypes: getResourceTypes(state)
  };
};
export default connect(mapStateToProps, { requestResourceTypes })(LibraryMenu);
