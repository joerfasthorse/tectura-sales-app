import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';

const QuanityInput = ({ fields, removeHandler, resources }) => {
  return (
    <div>
      {
        fields.map((name, index) => {
          const resource = resources[index];
          return (
            resource && resource.isSelected  &&<div key={name} className="selected-resources">
            { resource.thumbnail && 
              <img className="gallery-thumbnail" src={resource.thumbnail.src} alt={resource.thumbnail.title} />}
              <label><span dangerouslySetInnerHTML={{ __html: resource.title }}></span></label>
              <span>&nbsp;(<button onClick={() => removeHandler(index)}>remove</button>)</span>
              <Field component="input"
              className ="quanity"
                type="input"
                name={`${name}.quanity`} />
                
            </div>
          );
        })
      }
    </div>
  );
};

QuanityInput.propTypes = {
  types: PropTypes.array,
  typeName: PropTypes.string
};
export default QuanityInput;
