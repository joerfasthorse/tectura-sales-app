import React from 'react';
//import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import CSSTransition from 'react-transition-group';

const PageShell = Page => {
  return props =>
    <CSSTransition
      transitionName={props.transition ? props.transition : 'fade'}
      transitionAppear={true}
      transitionAppearTimeout={400}
      transitionEnter={true}
      transitionEnterTimeout={400}
      transitionLeave={true}
      transitionLeaveTimeout={1000}>
      <Page {...props} />
    </CSSTransition> 
};
export default PageShell;
