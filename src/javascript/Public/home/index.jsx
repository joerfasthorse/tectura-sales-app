import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Link,
} from 'react-router-dom';
const Home = ({ loginSuccessful }) => {
  return (
    <div className="body login">
	    <div className="main">
	    	<div className="content">
		      <h1 className="logo title"><img src="wp-content/themes/tectura-react/images/tectura_logo.svg" alt="Tectura"/></h1>
      {
        loginSuccessful ?
          <Link to="/app" className="button">Go To Dashboard</Link> : <Link to="/app-login" className="button">Login</Link>
      }
      		</div>
      	</div>
    </div>
  );
};

Home.propTypes = {
  loginSuccessful: PropTypes.bool
};
const mapStateToProps = state => {
  return {
    loginSuccessful: state.login.successful
  };
};
export default connect(mapStateToProps)(Home);
