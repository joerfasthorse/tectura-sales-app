import React from 'react';
import { Field } from 'redux-form';
const AssetSelector = ({ fields, allAssets, assetType }) => {
  return (
    <div className="grid">
      {
        fields.map((item, index) => {
          const asset = allAssets[index];
          const elemId = `asset-${asset.id}`;
          return (
            <div key={asset.id} className="grid-item selector">
              <div className={assetType + "-wrapper thumbnail-wrapper"}>
                <label htmlFor={elemId}>

                  <Field component="input"
                    type="checkbox"
                    id={elemId}
                    name={`${item}.isSelected`} />
                  <span className="checkbox icon-check"></span>
                </label>
                {
                  asset.image ?
                    <img className="gallery-thumbnail" src={asset.image} alt={asset.title} /> :
                    <span className={'icon-' + assetType}></span>
                }
              </div>
              <div>
                {(assetType === 'file' || assetType === 'video') && <h3 className="">{asset.title}</h3>}
              </div>
            </div>
          );
        })
      }
    </div>
  );
}
export default AssetSelector;
