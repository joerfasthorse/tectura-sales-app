import FetchApi from '../../helpers/fetch-helper';
const postUrl = '/wp-json/tectura/request/submit';

export  const submitRequest = async (request) => {
  try{
    const data =  await FetchApi({
      url: postUrl,
      method: 'POST',
      body: request
    })
    return data;
  } catch(err){
    throw err;
  }
}

const getResourceTypesUrl = '/wp-json/wp/v2/resource_type';
export  const fetchResourceTypes = async () => {
  try{
    const data =  await FetchApi({
      url: getResourceTypesUrl
    })
    return data && data.filter(type => type.is_request === '1');
  } catch(err){
    throw err;
  }
}

const getResourcesUrl = '/wp-json/wp/v2/resources?filter[resource_type]=';
export const fetchResources = async (resourceType) => {
  try{
    const data =  await FetchApi({
      url: getResourcesUrl + resourceType
    })
    return data;
  } catch(err){
    throw err;
  }
}