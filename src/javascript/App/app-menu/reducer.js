import {
  SET_APP_MENU
} from './constants';

const initialState = {
  pageLinks: [
    { name: 'Home', url: '/app'},
    { name: 'Library', url: '/app/library', isLibrary: true, unrestricted: true},
    { name: 'Request', url: '/app/request' },
    { name: 'News', url: '/app/news', isNews: true},
    { name: 'Colors', url: 'http://www.tecturadesigns.com/search-by-color', isExternal: true },
    { name: 'Tectura U', url: 'http://www.tecturadesigns.com/university', isExternal: true },
    // { name: 'Account', url: '/app/account' }
  ]
};

const reducer = function loginReducer(state = initialState, action) {
  switch (action.type) {
    case SET_APP_MENU:
      return action.data;
    default:
      return state;
  }
};

export default reducer;