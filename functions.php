<?php

define('TECTURA_NEW_RESOURCES', 'tectura_new_resources');
define('TECTURA_NEW_POSTS', 'tectura_new_posts');

// /wp-json/tectura/request/submit REQUEST TYPE : POST
require_once("wp-api/submit-request.php");
// /wp-json/tectura/library REQUEST TYPE : GET
require_once("wp-api/get-library.php");
require_once("wp-api/enable-rest-filter.php");
require_once("wp-api/library-share.php");
require_once("wp-api/new-resource-management.php");
// Remove all default WP template redirects/lookups
remove_action('template_redirect', 'redirect_canonical');

// Remove wp admin bar
add_filter('show_admin_bar', '__return_false');

// Redirect all requests to index.php so the react app is loaded and 404s aren't thrown
function remove_redirects() {
    add_rewrite_rule('^/(.+)/?', 'index.php', 'top');
}
add_action('init', 'remove_redirects');

// Load scripts
function load_react_scripts() {
    wp_enqueue_script('tecture/bundle.js', get_template_directory_uri() . '/build/bundle.js', null, null, true);
}
add_action('wp_enqueue_scripts', 'load_react_scripts', 100);

function tectura_styles() {
  wp_enqueue_style( 'tecture/style.css', get_template_directory_uri() . '/build/style.css', array(), date("H:i:s"));
}

add_action( 'wp_enqueue_scripts', 'tectura_styles' );


add_theme_support( 'post-thumbnails' ); 



//JOES ADDITIONS

function custom_login() { 
echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('stylesheet_directory').'/custom-login.css" />'; 
}
add_action('login_head', 'custom_login');

function my_login_logo_url() {
    return 'http://connect.tecturadesigns.com';
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_links() {
    echo '<p id="assistance"><a href="mailto:joer@fasthorseinc.com">Email for assistance</a></p>'; 
}
add_filter( 'login_form', 'my_login_links' );

function redirect_users_by_role() {
 
    if ( ! defined( 'DOING_AJAX' ) ) {
 
        $current_user   = wp_get_current_user();
        $role_name      = $current_user->roles[0];
 
        if ( 'subscriber' === $role_name ) {
            wp_redirect( 'http://connect.tecturadesigns.com/app' );
        } // if $role_name
 
    } // if DOING_AJAX
 
} // redirect_users_by_role
add_action( 'admin_init', 'redirect_users_by_role' );

add_filter( 'auto_update_core', '__return_true' );
add_filter( 'auto_update_plugin', '__return_true' );

// Register Custom Post Type
/*
function resources() {

	$labels = array(
		'name'                  => _x( 'Resources', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Resource', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Resources', 'text_domain' ),
		'name_admin_bar'        => __( 'Resources', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Resource', 'text_domain' ),
		'description'           => __( 'Resources', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array( 'resource-type'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-carrot',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 				=> array('slug' => 'library','with_front' => false),
		'show_in_rest' 			=> true,  //THIS IS NECESSARY FOR API
		'rest_controller_class' => 'WP_REST_Posts_Controller', //KHOA IS THIS RIGHT?
	);
	register_post_type( 'resource', $args );

}
// add_action( 'init', 'resources', 0 );


// Register Custom Post Type
function requests() {

	$labels = array(
		'name'                  => _x( 'Requests', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Request', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Requests', 'text_domain' ),
		'name_admin_bar'        => __( 'Requests', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Request', 'text_domain' ),
		'description'           => __( 'Requests', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-carrot',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' 				=> array('slug' => 'requests','with_front' => false),
		'show_in_rest' 			=> true,  //THIS IS NECESSARY FOR API
		'rest_base'          	=> 'requests',
		'rest_controller_class' => 'WP_REST_Posts_Controller', //KHOA IS THIS RIGHT?
	);
	register_post_type( 'request', $args );

}
// add_action( 'init', 'requests', 0 );

*/
//END JOES ADDITIONS

//Get image URL
function get_thumbnail_url($post){
    if(has_post_thumbnail($post['id'])){
        $imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $post['id'] ), 'full' ); // replace 'full' with 'thumbnail' to get a thumbnail
        $imgURL = $imgArray[0];
        return $imgURL;
    } else {
        return false;
    }
}
//integrate with WP-REST-API
function insert_thumbnail_url() {
     register_rest_field( 'post',
                          'featured_image',  //key-name in json response
                           array(
                             'get_callback'    => 'get_thumbnail_url',
                             'update_callback' => null,
                             'schema'          => null,
                             )
                         );
     }
//register action
add_action( 'rest_api_init', 'insert_thumbnail_url' );


add_filter('jwt_auth_token_before_dispatch', 'add_user_info', 10, 2);

function add_user_info($data, $user)
{
		$data['user_id'] = $user->data->ID;
		error_log(print_r($user->roles, true));
		$data['isRestricted'] = in_array( 'author',$user->roles) ? true : false;
    return $data;
}

// wp_enqueue_script( 'wp-api' );