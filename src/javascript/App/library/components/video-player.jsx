import React from 'react';
import PropTypes from 'prop-types';
import YouTube from '@u-wave/react-youtube';
class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isShow: true };
    this.onNext = this.onNext.bind(this);
    this.onPrev = this.onPrev.bind(this);
  }
  onNext(e){
    this.setState({isShow: false}, this.props.findNext(e))
    //settime to give time to reset the YOUTUBE component.
    setTimeout(()=> this.setState({isShow: true}), 300)
  }
  onPrev(e){
    this.setState({isShow: false},this.props.findPrev(e))
    //settime to give time to reset the YOUTUBE component.
    setTimeout(()=> this.setState({isShow: true}), 300)
  }
  render() {
    const { hasNext, hasPrev, asset, currentIndex, total, closeHandler, isOpen } = this.props;
    if (!isOpen || !asset) {
      return null;
    }
    return (
      <div className="image-viewer video">
        <div className="view-body">
          <div className="counter">
            <span>{currentIndex + 1}</span> of <span>{total}</span>
          </div>
          <div className="toggle-view">
            <span onClick={() => closeHandler()} className="icon-close"></span>
          </div>
          {hasPrev &&
            <a href="#"
              className="prev"
              onClick={this.onPrev}><span className="icon-arrow-left"></span></a>}
          {hasNext &&
            <a href="#"
              className="next"
              onClick={this.onNext}><span className="icon-arrow-right"></span></a>}
          {asset && this.state.isShow && <YouTube
            video={asset.videoId}
            width={640}
            height={480}
            autoplay
            showRelatedVideos={false}
          />
          }
        </div>
      </div>
    );
  }
}

VideoPlayer.propTypes = {
  asset: PropTypes.object,
  currentIndex: PropTypes.number,
  total: PropTypes.number,
  closeHandler: PropTypes.func,
  isOpen: PropTypes.bool,
  hasNext: PropTypes.bool,
  hasPrev: PropTypes.bool,
  findNext: PropTypes.func,
  findPrev: PropTypes.func,
}

export default VideoPlayer;