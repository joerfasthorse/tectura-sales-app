import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition, TransitionGroup } from 'react-transition-group'

const PageFade = (props) => (
  <CSSTransition
    {...props}
    classNames="fadeTranslate"
    timeout={1000}
    mountOnEnter={true}
    unmountOnExit={true}
  />
)

const ImageViewer = (props) => {
  const { hasNext, hasPrev, findNext, findPrev, asset, currentIndex, total, closeHandler, isOpen } = props;
  if (!isOpen || !asset) {
    return null;
  }
  return (
    <div className="image-viewer">
      <div className="view-body">
        <div className="counter">
          <span>{currentIndex + 1}</span> of <span>{total}</span>
        </div>
        <div className="toggle-view">
          <span onClick={() => closeHandler()} className="icon-close"></span>
        </div>
        {hasPrev &&
          <a href="#"
            className="prev"
            onClick={findPrev}><span className="icon-arrow-left"></span></a>}
        {hasNext &&
          <a href="#"
            className="next"
            onClick={findNext}><span className="icon-arrow-right"></span></a>}
        <TransitionGroup className="todo-list">
          <PageFade key={asset.fullImage}>
            <img src={asset.fullImage} />
          </PageFade>
        </TransitionGroup>
      </div>
    </div>
  );
};

ImageViewer.prototype = {
  asset: PropTypes.asset,
  currentIndex: PropTypes.number,
  total: PropTypes.number,
  closeHandler: PropTypes.func,
  isOpen: PropTypes.bool,
  hasNext: PropTypes.bool,
  hasPrev: PropTypes.bool,
  findNext: PropTypes.func,
  findPrev: PropTypes.func,
}

export default ImageViewer;