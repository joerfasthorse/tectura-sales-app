import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getPostById } from './selectors';
import { requestPostsData, updateViewedPosts } from './actions';
import { renderPostDate } from './../../helpers/data-helpers'

class NewsPost extends React.Component {
  componentDidMount() {
    const { post, match} = this.props;
    if(!post){
      this.props.requestPostsData();
    }
    this.props.updateViewedPosts(match.params.id);

  }

  renderPost(post) {
    return (
      post && <div className="body news">
                <div className="page-header">
                  <h2><NavLink className="news-btn" to="/app/news/">News</NavLink><span className="title">{post.title.rendered}</span></h2>
                </div>
                <div className="singlepost">
                  <h3>{post.title.rendered}</h3>
                  <h4>{renderPostDate(post.date)}</h4>
                  <img className="postimage" src={post.featured_image} />
                  <div dangerouslySetInnerHTML={{ __html: post.content.rendered }}>
                  </div>
                </div>
              </div>
    );
  }
  render() {
    const { post } = this.props;
    return (
      this.renderPost(post) || <div>Loading</div>
    )
  }
}


NewsPost.propTypes = {
  post: PropTypes.object,
  requestPostsData: PropTypes.func
};
const mapStateToProps = (state, ownProps) => {
  return {
    post: getPostById(state, ownProps.match.params.id),
  };
};
export default connect(mapStateToProps, { requestPostsData, updateViewedPosts })(NewsPost);
