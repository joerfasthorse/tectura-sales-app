import * as Validate from './../../helpers/form-validation-rules';

const validate = values => {
  const errors = {};
  if (!values.requestFor) {
    errors.requestFor = 'Required';
  }
  if (!values.resourceType) {
    errors.resourceType = 'Required';
  }
  if (!values.recipients || !values.recipients.length) {
    errors.recipients = { _error: 'At least one recipient must be entered' };
  } else {
    const recipientsArrayErrors = [];
    values.recipients.forEach((recipient, recipientIndex) => {
      const recipientErrors = {};
      if (!recipient || !recipient.firstName) {
        recipientErrors.firstName = 'Required';
        recipientsArrayErrors[recipientIndex] = recipientErrors;
      }
      if (!recipient || !recipient.lastName) {
        recipientErrors.lastName = 'Required';
        recipientsArrayErrors[recipientIndex] = recipientErrors;
      }
      if (!recipient || !recipient.email) {
        recipientErrors.email = 'Required';
        recipientsArrayErrors[recipientIndex] = recipientErrors;
      }
      if (recipient.email && !Validate.email(recipient.email)) {
        recipientErrors.email = 'Invalid Email';
        recipientsArrayErrors[recipientIndex] = recipientErrors;
      }
      if (recipient.zipcode && !Validate.zipcode(recipient.zipcode)) {
        recipientErrors.zipcode = 'Invalid Zipcode';
        recipientsArrayErrors[recipientIndex] = recipientErrors;
      }
    });
    if (recipientsArrayErrors.length) {
      errors.recipients = recipientsArrayErrors;
    }
  }
  return errors;
};

export default validate;
