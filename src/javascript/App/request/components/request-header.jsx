import React from 'react';
import PropTypes from 'prop-types';
const STEP_TWO = 2;
const RequestHeader = ({ breadcrumbs, currentStep }) => {
  return (
    <div className="page-header">
      <div className="breadcrumb">
        <a className="btn">Request</a>
        {
          currentStep === STEP_TWO && breadcrumbs && breadcrumbs.map((item, index) => {
            return (
              <span key={index} ><a className="link"> <span className="icon icon-arrow-right"></span> {item} </a></span>
            );
          })
        }
      </div>
    </div>
  );
};


RequestHeader.propTypes = {
  breadcrumb: PropTypes.array
};
export default RequestHeader;