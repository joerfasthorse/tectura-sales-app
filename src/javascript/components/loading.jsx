import React from 'react';

const Loading = () => {
  return (
    <div className="loading">
      <div className="c1 c"></div>
      <div className="c2 c"></div>
      <div className="c3 c"></div>
      <div className="c4 c"></div>
      <div className="c5 c"></div>
      <div className="c6 c"></div>
      <div className="c7 c"></div>
      <div className="c8 c"></div>
      <div className="c9 c"></div>
      <div className="c10 c"></div>
      <div className="c11 c"></div>
      <div className="c12 c"></div>
      <div className="text">...loading...</div>
    </div>
  )
}

export default Loading;