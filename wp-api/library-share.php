<?php

add_filter( 'wp_mail_from', 'my_mail_from' );
function my_mail_from( $email ) {
	return "wtile@wausautile.com";
}
add_filter( 'wp_mail_from_name', 'my_mail_from_name' );
function my_mail_from_name( $name ) {
	return "wtile@wausautile.com";
}
add_filter('wp_mail_content_type','set_content_type');
function set_content_type($content_type){
	return 'text/html';
}

add_action( 'rest_api_init', 'tectura_libary_share_submit' );
function tectura_libary_share_submit() {
	register_rest_route( 'tectura', '/library/submitshare', array(
		        'methods' => 'POST',
		        'callback' => 'tectura_libary_share_submit_callback',
		        'permission_callback' => function ($request) {
		//if (current_user_can('edit_others_posts'))
				            return true;
	}
	));
}
function tectura_libary_share_submit_callback( $request_data ) {
	//i	nsert the post and set the category
		$data = $request_data->get_json_params();
	
	$to = $data['recipients'];
	$subject = 'You have files from Tectura Connect';
	$message = '<h2>Here are the files you requested:</h2>';
	$message .= '<h3>'.$data['resourceName'].'</h3>';
  if($data['dropboxFolder']){
		$message .= '<strong>Dropbox Folder: </strong><a href="'.$data['dropboxFolder'].'">'.$data['dropboxFolder'].'</a><br/>';
	}
  $headers[] = 'From: Wausau Tile <wtile@wausautile.com>';
  $headers[] = 'Content-Type: text/html; charset=UTF-8';
  
  $files = '';
  foreach($data['assets'] as &$asset){
		$link = buildImageLink($asset);
		if($link){
			$files .= $link.'</br>';
		}
	}
	if($files){
		$files = '<strong>Images:</strong></br>'.$files;
	}

	$videos = '';
  foreach($data['assets'] as &$asset){
		$link = buildVideoLink($asset);
		if($link){
			$videos .= $link.'</br>';
		}
	}
	if($videos){
		$videos = '<strong>Videos:</strong></br>'.$videos;
	}
	
	$message = $message.'</br>'.$files.$videos;
	$message .= '<p>'.$data['comments'].'<p>';
	wp_mail($to, $subject,$message, $headers);
	return $data;
}

function buildImageLink($asset){
	if($asset['link']){
		return '<a href="'.$asset['link'].'">'.$asset['link'].'</a>';
	}
	return null;
}
function buildVideoLink($asset){
	if($asset['videoId']){
		return '<a href="https://youtu.be/'.$asset['videoId'].'">https://youtu.be/'.$asset['videoId'].'</a>';
	}
	return null;
}