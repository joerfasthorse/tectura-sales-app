import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group'

import ImageViewer from './image-viewer';
import VideoPlayer from './video-player';

const PageFade = (props) => (
  <CSSTransition
    {...props}
    classNames="fadeTranslate"
    timeout={1000}
    mountOnEnter={true}
    unmountOnExit={true}
  />
)
class ResourceGallery extends React.Component {
  constructor(props) {
    super(props);
    this.state = { currentIndex: null, viewerIsOpen: false };
    // this.closeModal = this.closeModal.bind(this);
    this.findNext = this.findNext.bind(this);
    this.findPrev = this.findPrev.bind(this);
    // this.renderImageContent = this.renderImageContent.bind(this);
    this.openViewer = this.openViewer.bind(this);
    this.closeViewer = this.closeViewer.bind(this);
    this.escFunction = this.escFunction.bind(this);
  }
  escFunction(event) {
    if (event.keyCode === 27) {
      //Do whatever when esc is pressed
      this.closeViewer();
    }
  }
  componentDidMount() {
    document.addEventListener('keydown', this.escFunction, false);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.escFunction, false);
  }
  findPrev(e) {
    if (e != undefined) {
      e.preventDefault();
    }
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex - 1
    }));
  }
  findNext(e) {
    if (e != undefined) {
      e.preventDefault();
    }
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1
    }));
  }
  openViewer(index) {
    this.setState({ currentIndex: index, viewerIsOpen: true });
  }
  closeViewer() {
    this.setState({ currentIndex: null, viewerIsOpen: false });
  }
  render() {
    const { assets, assetType } = this.props;
    // const goToPage = category => history.push(`${match.path}/${category}`);
    if (!assets) {
      return null;
    }
    return (
      <React.Fragment>
        <div className="image-gallery grid">
          {
            assets && assets.map((item, index) => {
              const className = `thumbnail-wrapper ${assetType}`;
              return (
                <div key={item.id} className="grid-item" onClick={() => assetType === 'gallery' && this.openViewer(index)}>
                  <div className={className}>
                    {
                      item.image ? 
                      <img className="gallery-thumbnail" src={item.image} alt={item.title} /> :
                      <span className={'icon-' + assetType}></span>
                    }
                  </div>
                  <div>
                      {(assetType === 'file' || assetType === 'video') && <h3 className="">{item.title}</h3>}
                      {/* {assetType === 'file' && <h4>{item.fileExt}</h4>} */}
                      {assetType === 'file' && <a href={item.link} className="btn" target="_blank">Download</a>}
                      {assetType === 'video' && <button className="" onClick={() => this.openViewer(index)}>Watch Video</button>}
                  </div>
                </div>
              )
            })
          }
        </div>
        {
          assetType === 'gallery' &&
          <PageFade in={this.state.viewerIsOpen}>
          <ImageViewer
            isOpen={this.state.viewerIsOpen}
            findPrev={this.findPrev}
            findNext={this.findNext}
            hasPrev={this.state.currentIndex > 0}
            hasNext={this.state.currentIndex + 1 < assets.length}
            asset={assets[this.state.currentIndex]}
            currentIndex={this.state.currentIndex}
            total={assets.length}
            closeHandler={this.closeViewer}
          />
          </PageFade>
        }
        {
          assetType === 'video' && <VideoPlayer
            isOpen={this.state.viewerIsOpen}
            findPrev={this.findPrev}
            findNext={this.findNext}
            hasPrev={this.state.currentIndex > 0}
            hasNext={this.state.currentIndex + 1 < assets.length}
            asset={assets[this.state.currentIndex]}
            currentIndex={this.state.currentIndex}
            total={assets.length}
            closeHandler={this.closeViewer}
          />
        }
      </React.Fragment>
    );
  }
}

ResourceGallery.propTypes = {
  assets: PropTypes.array
};
export default ResourceGallery
