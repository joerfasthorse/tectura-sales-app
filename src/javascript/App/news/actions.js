import {
  REQUEST_POSTS_DATA,
  UPDATE_POST_NOTIFICTION
} from './constants';

// In order to perform an action of type LOGIN_REQUESTING
// we need an email and password

export const requestPostsData = () => ({type: REQUEST_POSTS_DATA});
export const updateViewedPosts = postId => ({type: UPDATE_POST_NOTIFICTION, postId})