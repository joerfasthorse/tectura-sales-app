import React from 'react';
import { connect } from 'react-redux';
import {
  Route,
  Switch
} from 'react-router-dom';
// import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
// import TransitionGroup from 'react-transition-group/TransitionGroup';
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import AppMenu from './app-menu';
import Home from './home';
import Library from './library';
import LibResourceType from './library/components/resource-type';
import LibResource from './library/components/resource';
import { requestUserNotifications } from './library/actions';
import Request from './request';
import News from './news';
import NewsPost from './news/post';
import LibMenu from './library/components/library-menu';
// import NewsMenu from './news/news-menu';
import Account from './account';
import NotFound from '../components/404';
import { getCurrentUser } from './library/selectors';

const PageFade = (props) => (
  <CSSTransition
    {...props}
    classNames="fadeTranslate"
    timeout={1000}
    mountOnEnter={true}
    unmountOnExit={true}
  />
)

class App extends React.PureComponent {
  componentWillMount() {
    this.props.requestUserNotifications();
  }
  render() {
    const { match, location, user } = this.props;
    const locationKey = location.pathname
    if (user.isRestricted) {
      return (
        <div key={locationKey}>
          <Switch>
            <Route path={`${match.url}/library/:resourceType`} component={LibMenu} />
            <Route path={match.url} component={AppMenu} />
          </Switch>
          <TransitionGroup>
            <PageFade key={locationKey}>
              <section className="fix-container">
                <Switch location={location}>
                  <Route exact path={`${match.url}/library/:resourceType/:id`} component={LibResource} />
                  <Route exact path={`${match.url}/library/:resourceType`} component={LibResourceType} />
                  <Route exact path={`${match.url}/library`} component={Library} />
                  <Route exact path={match.url} component={Home} />
                </Switch>
              </section>
            </PageFade>
          </TransitionGroup>
        </div>
      )
    }
    return (
      <div>
        <Switch>
          <Route path={`${match.url}/library/:resourceType`} component={LibMenu} />
          {/* <Route path={`${match.url}/news/:id`} component={NewsMenu} /> */}
          <Route path={match.url} component={AppMenu} />
        </Switch>
        <TransitionGroup>
          <PageFade key={locationKey}>
            <section className="fix-container">
              <Switch location={location}>
                <Route exact path={`${match.url}/library/:resourceType/:id`} component={LibResource} />
                <Route exact path={`${match.url}/library/:resourceType`} component={LibResourceType} />
                <Route exact path={`${match.url}/library`} component={Library} />
                <Route exact path={`${match.url}/request`} component={Request} />
                <Route exact path={`${match.url}/news/:id`} component={NewsPost} />
                <Route exact path={`${match.url}/news`} component={News} />
                <Route exact path={`${match.url}/account`} component={Account} />
                <Route exact path={match.url} component={Home} />
                <Route component={NotFound} />
              </Switch>
            </section>
          </PageFade>
        </TransitionGroup>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: getCurrentUser(state),
  };
};
export default connect(mapStateToProps, { requestUserNotifications })(App)
