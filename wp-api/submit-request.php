<?php
add_action( 'rest_api_init', 'tectura_request_submit' );
function tectura_request_submit() {
	register_rest_route( 'tectura', '/request/submit', array(
	        'methods' => 'POST',
	        'callback' => 'tectura_request_submit_callback',
	        'permission_callback' => function ($request) {
		//if (current_user_can('edit_others_posts'))
		            return true;
	}
	));
}
function tectura_request_submit_callback( $request_data ) {
	//i	nsert the post and set the category
	$data = $request_data->get_json_params();
	$metaInputs = array(
		'request_for' => $data['requestFor'],
		'type' => $data['resourceType']['id']
	);
	$current_user = wp_get_current_user();
	  
	$subjectLine = ucfirst($data['type']).' '.'Request Submitted by'.' '.$current_user->user_login.' for';
	$emails = array("wtile@wausautile.com");
	$customsInformation = '';
	foreach ($data['recipients'] as $key=>$recipient) {
		array_push($emails, $recipient['email']);
		$name = ucfirst($recipient['firstName']).' '.ucfirst($recipient['lastName']);
		$address = $recipient['address'].' '.$recipient['state'].' '.$recipient['zipcode'];
		$email = $recipient['email'];
		$info = $name."\n".$address."\n".$email;
		$stringName = 'recipient_'.($key+1);
		$metaInputs[$stringName] = $info;
		if($key === 0){
			$subjectLine = $subjectLine.' '.$name;
		}
		$info = $name.'<br/>';
		$info .= $address.'</br/>';
		$info .= $email;
		$customsInformation .= '<p>'.$info.'</p>';
	}
	$emailSubject = 'Request submitted from Tectura Connect';
	$requestFor = '<strong>Request Submitted for:</strong> '.$data['requestFor'];
	$sampleType = '<strong>Sample Requested:</strong> '.$data['resourceType']['name'];

	$samples = '';
	foreach($data['resources'] as &$resource){
		$samples .= '<tr><td style="padding:0 15px 0 0;">'.$resource['title'].'</td><td style="padding:0 15px 0 0;">'.$resource['quanity'].'</td></tr>';
	}

	$samples = '<table><tr><th align="left">Name</th><th align="left">Quanity</th></tr>'.$samples.'</table>';

	$emailMessage = $requestFor.'<br/>'.$sampleType.'<br/>'.$samples.'<br/>'.$customsInformation;
	
	$headers[] = 'From: Wausau Tile <wtile@wausautile.com>';
	$headers[] = 'Content-Type: text/html; charset=UTF-8';

	wp_mail($emails, $emailSubject, $emailMessage, $headers);
	$post_id = wp_insert_post(array (
		'post_title' => $subjectLine,
		'post_type' => 'request_submission',
		'post_status' => 'publish',
		'comment_status' => 'closed',   // 	if you prefer
		'ping_status' => 'closed',      // 	if you prefer
		'meta_input' => $metaInputs
	));

	return $data;
}

function var_error_log( $object=null ){
	ob_start();
	// 	start buffer capture
	    var_dump( $object );
	// 	dump the values
	    $contents = ob_get_contents();
	// 	put the buffer into a variable
	    ob_end_clean();
	// 	end capture
	    error_log( $contents );
	// 	log contents of the result of var_dump( $object )
}
