import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { requestLogout } from './../../Public/login/actions';
import { getNewPosts, getNewResources, getCurrentUser } from './../library/selectors';


const AppMenu = props => {
  const { newPosts, newResources, user } = props;
  const resourcesCount = Object.keys(newResources).length;
  const logout = () => {
    props.requestLogout({
      callback: () => {
        props.history.push('/');
      }
    });
  };
  const navItems = props.navLinks && props.navLinks.map(item => {
    if(user.isRestricted && !item.unrestricted){
      return null;
    }
    return (
      <li key={item.name}>
        {
          item.isExternal
            ? <a href={item.url} target="_blank" className={'icon-' + item.name.replace(/\s+/g, '-').toLowerCase()}>{item.name}</a>
            : <NavLink
              to={item.url}
              className={'icon-' + item.name.replace(/\s+/g, '-').toLowerCase()}
              activeClassName="active">
              {item.name}
              {
                item.isLibrary && resourcesCount > 0 &&
                <span className="badge">
                  {resourcesCount}
                </span>
              }
              {
                item.isNews && newPosts.length > 0 &&
                <span className="badge">
                  {newPosts.length}
                </span>
              }
            </NavLink>
        }
      </li>
    );
  });
  return (
    <div className="main-navigation">
      <ul>
        {navItems}
      </ul>
      <button onClick={() => logout()}>Logout</button>
    </div>
  );
};

AppMenu.propTypes = {
  navLinks: PropTypes.array,
  requestLogout: PropTypes.func
};
const mapStateToProps = state => {
  return {
    navLinks: state.appMenu.pageLinks,
    newPosts: getNewPosts(state),
    newResources: getNewResources(state),
    user: getCurrentUser(state)
  };
};
export default connect(mapStateToProps, { requestLogout })(AppMenu);
