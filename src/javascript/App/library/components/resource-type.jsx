import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getFilterResources, getBreadcrumb, getProductTypes, getNewResources, getCurrentUser } from './../selectors';
import { requestResources, requestProductTypes, setSelectedProductType } from './../actions';
import { makeUrl } from './../../../helpers/helpers';
import LibraryHeader from './library-header'
import LoadingComponent from '../../../components/loading'
import NotFound from '../../../components/404';

class LibResourceType extends React.Component {
  constructor(props) {
    super(props);
    this.handleFilter = this.handleFilter.bind(this);
    // this.onScroll = this.onScroll.bind(this);
  }
  componentDidMount() {
    this.props.match.params.resourceType ? this.props.requestResources({
      resourceType: this.props.match.params.resourceType
    })
      : null
    if (this.props.productTypes.length === 0) {
      this.props.requestProductTypes()
    }
  }

  componentWillReceiveProps(nextProps) {
    nextProps.match.params.resourceType !== this.props.match.params.resourceType
      ? this.props.requestResources({
        resourceType: nextProps.match.params.resourceType
      })
      : null
  }
  handleFilter(value) {
    this.props.setSelectedProductType(value)
  }
  renderGrid() {
    const { history, match, resources, newResources } = this.props;
    const goToPage = id => history.push(`${makeUrl(match.url)}${id}`);
    return (
      resources && resources.map(resource => {
        const thumbnail = resource.thumbnail ? resource.thumbnail.image_url : '';
        const style = {
          backgroundImage: `url(${thumbnail})`
        }
        const isNew = newResources.hasOwnProperty(resource.id);
        return (
          <div className="flex-item"
            style={style}
            key={resource.id}
            onClick={() => goToPage(resource.id)}>
            {
              isNew && <span className="badge">new</span>
            }
            <h2 className="title">{resource.title.rendered}</h2>
            
          </div>
        );
      })
    )
  }
  render() {
    const { match, breadcrumbs, productTypes, user } = this.props;
    return (
      <div className="page-transition">
        <div className="body has-secondary-nav library" key={match.url}>
          <LibraryHeader 
            breadcrumbs={breadcrumbs} 
            resourceType={match.params.resourceType} 
            productTypes={productTypes} 
            handleFilter={this.handleFilter} 
            user={user} />
          <div className="main flex">
            {this.renderBody()}
          </div>
        </div>
      </div>
    );
  }
  renderBody() {
    const { resources, isFetching, error } = this.props;
    if (isFetching) {
      return <LoadingComponent />
    } else if (Object.keys(error).length !== 0) {
      if (error.data.status === 404) {
        return <NotFound />
      } else {
        return <div className="error">{error.message}</div>
      }
    } else if (Object.keys(resources).length !== 0) {
      return this.renderGrid();
    }
  }
}

LibResourceType.propTypes = {
  resources: PropTypes.array
};
const mapStateToProps = (state, props) => {
  return {
    isFetching: state.library.isFetchingResources,
    isFetchingMore: state.library.isFetchingMoreResources,
    error: state.library.resourcesError,
    resources: getFilterResources(state),
    productTypes: getProductTypes(state),
    breadcrumbs: getBreadcrumb(state, props.match.params.resourceType),
    newResources: getNewResources(state),
    user: getCurrentUser(state)

  };
};

export default connect(mapStateToProps, { requestResources, requestProductTypes, setSelectedProductType })(LibResourceType);
