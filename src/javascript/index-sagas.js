import loginSaga from './Public/login/sagas';
import requestSaga from './App/request/sagas';
import newsSaga from './App/news/sagas'
import librarySagas from './App/library/sagas'

export default function *IndexSaga() {
  yield [
    loginSaga(),
    requestSaga(),
    newsSaga(),
    librarySagas()
  ];
}