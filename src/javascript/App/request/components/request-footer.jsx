import React from 'react';
import PropTypes from 'prop-types';

const RequestFooter = (
  {
      hasNext,
    hasPrevious,
    hasSend,
    handleNext,
    handlePrevious,
    handleSend,
    enableNext,
    enableSend
    }) => {
  return (
    <div className="page-footer">
      {hasPrevious &&
        <button onClick={() => handlePrevious()} className="previous"><span className="icon icon-arrow-left"> Previous</span></button>}
      {hasNext &&
        <button className="next icon-arrow-right" disabled={!enableNext} onClick={() => handleNext()}>Next</button>}
      {hasSend &&
        <button className="submit icon-send" disabled={!enableSend} onClick={() => handleSend()}>Send</button>}
    </div>
  );
};


RequestFooter.propTypes = {
  hasNext: PropTypes.bool,
  hasPrevious: PropTypes.bool,
  hasSend: PropTypes.bool,
  handleNext: PropTypes.func,
  handlePrevious: PropTypes.func,
  handleSend: PropTypes.func
};
export default RequestFooter;