import FetchApi from '../../helpers/fetch-helper';
const postUrl = '/wp-json/wp/v2/posts';
export  const fetchPosts = async () => {
  try{
    const data =  await FetchApi({url: postUrl})
    return data;
  } catch(err){
    throw err;
  }
}

const viewedPostUrl = (userId, postId) => `/wp-json/tectura/user/viewed-post?user_id=${userId}&post_id=${postId}`;
export  const updateViewedPost = async (userId, postId) => {
  try{
    const data =  await FetchApi({url: viewedPostUrl(userId, postId)})
    return data;
  } catch(err){
    throw err;
  }
}
