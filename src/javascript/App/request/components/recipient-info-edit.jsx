import React from 'react';
import { Field } from 'redux-form';
import UsStatesJson from '../../../helpers/us-states.json';
import { renderSelect, renderInput } from '../../../helpers/form-helpers';
const RecipientInfoEdit = ({ fields }) => {
  return (
    <div className="recipient-wrapper">
      {fields.map((recipient, index) =>
        <div key={index} className="recipient-block">
          {
            fields.length > 1 && <button
              type="button"
              title="Remove Recipient"
              className="remove"
              onClick={() => fields.remove(index)}>Remove Recipient</button>
          }
        
            <Field
              name={`${recipient}.firstName`}
              type="text"
              component={renderInput}
              label="First Name"
              placeholder="First Name"/>
            <Field
              name={`${recipient}.lastName`}
              type="text"
              component={renderInput}
              label="Last Name"
              placeholder="Last Name"/>
       
            <Field
              name={`${recipient}.address`}
              type="text"
              component={renderInput}
              label="Address"
              placeholder="Adress" />
            <Field name={`${recipient}.state`} component={renderSelect}>
              <option value="" disabled>State</option>
              {
                UsStatesJson && UsStatesJson.map(state => {
                  return (
                    <option
                      key={state.abbreviation}
                      value={state.abbreviation}>
                      {state.abbreviation}
                    </option>
                  );
                })
              }
            </Field>
            <Field
              name={`${recipient}.zipcode`}
              type="text"
              component={renderInput}
              label="Zip Code"
              placeholder="Zip Code"/>
       
            <Field
              name={`${recipient}.email`}
              type="email"
              component={renderInput}
              label="Email"
              placeholder="Email" />
     
        </div>
      )}
      <button type="button" className="add icon-close" onClick={() => fields.push({})}>Add another recipient</button>
    </div>
  );
};

export default RecipientInfoEdit;
