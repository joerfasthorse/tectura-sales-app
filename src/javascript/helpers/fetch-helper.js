const getAuthToken = () => {
  const authUser = JSON.parse(window.localStorage.getItem('auth_user')) || null;
  return authUser && authUser.token ? authUser.token : null;
}
// const checkError = resp => {
//   return resp.ok ? { error: false } : { error: true, message: resp.statusText, code: resp.status }
// }
export default async (options) => {
  const headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }
  const token = getAuthToken();
  if(token){
    headers['Authorization'] = `Bearer ${token}`;
  }
  try {
    const fetchOptions = {
      method: options.method ? options.method : 'GET',
      body: options.body ? JSON.stringify(options.body) : undefined,
      headers: options.headers ? new Headers(Object.assign(options.headers, headers)) : new Headers(headers)
    }

    const resp = await fetch(options.url, fetchOptions)
    if(!resp.ok){
      throw await resp.json();
    }
    if(options.returnHeaders){
      return {
        headers: resp.headers,
        data: await resp.json()
      }
    }
    return await resp.json();
  } catch (err) {
    throw await err
  }
}