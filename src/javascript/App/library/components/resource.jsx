import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getResourceViwing, getBreadcrumb, getCurrentUser } from './../selectors';
import { requestResourceById } from './../actions';
import ResourceGallery from './resource-gallery';
import ResourceShare from './resouce-share';
import { setIsShareMode } from './../actions';
import LibraryHeader from './library-header'
import LoadingComponent from '../../../components/loading'
import NotFound from '../../../components/404';

class LibResource extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    document.getElementById('tectura-app').scrollIntoView();
    this.props.requestResourceById(this.props.match.params.id)
  }
  componentWillUnmount() {
    this.props.setIsShareMode(false);
  }
  render() {
    const { resource, isShare, setIsShareMode, breadcrumbs, match, user } = this.props;
    return (
      <div className="page-transition">
        <div className="body has-secondary-nav library">
          <LibraryHeader
            breadcrumbs={breadcrumbs}
            user={user}
            hasShareBtn={true}
            setIsShareMode={setIsShareMode}
            isShare={isShare}
            resourceType={match.params.resourceType}
            downloadLink={resource ? resource.dropbox_folder : null} />
          <div className="main">
            {this.renderBody()}
          </div>
        </div>
      </div>
    )
  }
  renderBody() {
    const { resource, isFetching, error } = this.props;
    if (isFetching) {
      return <LoadingComponent />
    } else if (Object.keys(error).length !== 0) {
      if (error.data.status === 404) {
        return <NotFound />
      } else {
        return <div className="error">{error.message}</div>
      }
    } else if (Object.keys(resource).length !== 0) {
      return this.renderGrid();
    }
  }
  renderGrid() {
    const { resource, isShare } = this.props;
    return isShare ?
      <ResourceShare assetType={resource.asset_type} initialValues={
        {
          assets: resource.assets,
          assetType: resource.asset_type,
          isSelectAll: false,
          recipients: [{}],
          resourceName: resource.title.rendered,
          dropboxFolder: resource.dropbox_folder
        }
      } /> :
      <ResourceGallery assetType={resource.asset_type} assets={resource.assets} />
  }
}

LibResource.propTypes = {
  resource: PropTypes.object
};
const mapStateToProps = (state, ownProps) => {
  return {
    isFetching: state.library.isFetchingResourceById,
    error: state.library.resourceViewingError,
    resource: getResourceViwing(state),
    isShare: state.library.isShareMode,
    breadcrumbs: getBreadcrumb(state, ownProps.match.params.resourceType, ownProps.match.params.id),
    user: getCurrentUser(state)
  };
};

export default connect(mapStateToProps, { requestResourceById, setIsShareMode })(LibResource);
