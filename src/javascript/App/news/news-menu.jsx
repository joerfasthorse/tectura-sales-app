import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
const NewsMenu = props => {
  const { history } = props;
  return (
    <div className="main-navigation">
      <ul>
          <li><button className="back-arrow" onClick={() => history.goBack()}><span className="icon-arrow-left"></span></button></li>
       	  <li key="Home">
            <NavLink to="/app" className="icon-home" activeClassName="active">Home</NavLink>
          </li>
          <li key="Library">
            <NavLink to="/app/library" className="icon-library" activeClassName="active">Library</NavLink>
          </li>
          <li key="Request">
            <NavLink to="/app/request" className="icon-request" activeClassName="active">Request</NavLink>
          </li>
         </ul>
    </div>
  );
  
};
export default connect()(NewsMenu);
