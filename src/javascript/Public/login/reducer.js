import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  CHECK_AUTHENTICATION,
  VALIDATE_TOKEN,
} from './constants';

const authUser = window.localStorage.getItem('auth_user') || null;

const initialState = {
  isFetching: true,
  isAuthenticated: authUser ? true : false,
  user: authUser ? JSON.parse(authUser) : {},
  errors: []
};

const reducer = function loginReducer(state = initialState, action) {
  switch (action.type) {
    // Set the requesting flag and append a message to be shown
    case LOGIN_REQUEST:
      return {
        ...state,
        isFetching: true,
        isAuthenticated: false,
        errorMessage: ''
      };

    // Successful?  Reset the login state.
    case LOGIN_FAILURE:
      return {
        ...state,
        isFetching: false,
        isAuthenticated: false,
        errorMessage: action.message
      };

    // Append the error returned from our api
    // set the success and requesting flags to false
    case LOGIN_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isAuthenticated: true,
        user: action.user,
        errorMessage: ''
      };
    case LOGOUT_REQUEST:
      return {
        ...state,
        isFetching: true
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isAuthenticated: false,
        user: null,
        errorMessage: ''

      };
    case CHECK_AUTHENTICATION:
      return {
        ...state,
        isFetching: true,
        isAuthenticated: false
      }
    case VALIDATE_TOKEN:
      return {
        ...state,
        isFetching: false,
        isAuthenticated: true,
        errorMessage: '',
      }
    default:
      return state;
  }
};

export default reducer;