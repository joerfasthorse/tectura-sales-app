import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { getHomeNavLink } from './selector';
import { getNewPosts, getNewResources, getCurrentUser } from './../library/selectors';

const Home = ({ newPosts, newResources, navLinks, history, user }) => {
  const goToPage = link => link.isExternal ? window.open(link.url, '_blank') : history.push(link.url);
  const resourcesCount = Object.keys(newResources).length;
  if(user.isRestricted){
    return <Redirect to="/app/library"/>
  }
  return (
    <div className="page-transition">
      <div className="body home">
        <div className="main">
          {
            navLinks && navLinks.map(link => {
              if(user.isRestricted && !link.unrestricted){
                return null;
              }
              return (
                <div className={'flex-item ' + link.name.toLowerCase()} key={link.url} onClick={() => goToPage(link)}>
                {
                link.isLibrary && resourcesCount > 0 && <span className="badge">{resourcesCount} new</span>
              	}
              	{
                link.isNews && newPosts.length > 0 && <span className="badge">{newPosts.length} new</span>
              	}
                  <h2 className="title">{link.name}</h2>
                </div>
              );
            })
          }
        </div>
      </div>
    </div>
  );
};

Home.propTypes = {
  navLinks: PropTypes.array
};
const mapStateToProps = state => {
  return {
	newPosts: getNewPosts(state),
    newResources: getNewResources(state),
    navLinks: getHomeNavLink(state),
    user: getCurrentUser(state)
  };
};

export default connect(mapStateToProps)(Home);
