import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { renderCheckbox, } from '../../../helpers/form-helpers';

const ResourceSelector = ({ fields }) => {
  const allResources = fields.getAll();

  return (
    <div className="resource-grid">
      {
        fields.map((name, index) => {
          const resource = allResources[index];
          return (
            <div key={name} className="selected-resources">
            { resource.thumbnail && 
            <div className="gallery-thumbnail"><img src={resource.thumbnail.src} alt={resource.thumbnail.title} /></div>}
              <label htmlFor={`resource-${index}`}>
                <Field component={renderCheckbox}
                  id={`resource-${index}`}
                  type="checkbox"
                  label={resource.title}
                  name={`${name}.isSelected`} />
              </label>
              
            </div>
          );
        })
      }
    </div>
  );
};

ResourceSelector.propTypes = {
  types: PropTypes.array,
  typeName: PropTypes.string
};
export default ResourceSelector;
