import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
const LibraryHeader = ({ breadcrumbs, hasShareBtn, setIsShareMode, isShare, resourceType, productTypes, handleFilter, downloadLink }) => {

  return (
    breadcrumbs.length > 0 && <div className="page-header">
      <div className="breadcrumb">
        {/* {user && user.isRestricted && <NavLink className="btn" to="/app/galleries">galleries</NavLink>} */}
        {breadcrumbs && breadcrumbs[0] && <NavLink className="btn" to={`/app/library/${resourceType}`}>{breadcrumbs[0]}</NavLink>}
        {breadcrumbs && breadcrumbs[1] && <span><a className="link"> <span className="icon icon-arrow-right"></span> {breadcrumbs[1]} </a></span>}

        {hasShareBtn && <button className="share" onClick={() => setIsShareMode(!isShare)}><span className={isShare ? 'icon-close' : 'icon-share'}></span></button>}
        {downloadLink && <a href={downloadLink} target="_blank" className="button download"><span className="icon-download"></span></a>}
        {productTypes && <div className="product-type">
          <label>Product Type:
            <select onChange={e => handleFilter(e.target.value)}>
              <option value="all">All Product Categories</option>
              {
                productTypes.map(p =>
                  <option key={p.slug} value={p.id}>{p.name}</option>
                )
              }

            </select>
            <span className="icon-down"></span>
          </label>
        </div>
        }
      </div>
    </div>
  );
};


LibraryHeader.propTypes = {
  breadcrumb: PropTypes.array
};
export default LibraryHeader;