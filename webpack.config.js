const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    filename: './index.html',
    template: './index.html'
});
module.exports = {
    cache: true,
    devtool: 'sourcemap',
    entry: './src/javascript/index.jsx',
    output: {
        path: `${__dirname}/build`,
        publicPath: '/',
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        }, {
            test: /\.jsx$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        }, {
            test: /\.less$/,
            include: /styles/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'css-loader',
                    options: { url: false }
                }, {
                    loader: 'less-loader'
                }],
            }),
        }
        ]
    },
    devServer: {
        index: 'index.html',
        historyApiFallback: true,
        proxy: {
            '/wp-json': {
                target: 'http://local.tectura.com'
            }
        }
    },
    plugins: [HtmlWebpackPluginConfig,new ExtractTextPlugin('style.css')]

};
