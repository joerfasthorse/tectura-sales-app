import React from 'react';
const AssetSelectorReview = ({fields, removeHandler, allAssets, meta: { error, warning }}) => {
  return (
    <div>
      {
        fields.map((item, index) => {
          const asset = allAssets[index];
          return (
            asset && asset.isSelected && <div key={asset.id} className="asset-view">
            {
              asset.image ?
                <img src={asset.image} alt={asset.title} /> :
                <span className="icon-file review-page"></span>
            }

              {asset.title}
              <span>&nbsp;(<button onClick={() => removeHandler(index)}>remove</button>)</span>
            </div>
          );
        })
      }
      {((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
  );
}
export default AssetSelectorReview;
