import _ from 'lodash';
import { formValueSelector, getFormSyncErrors } from 'redux-form';
import { REQUEST_FORM_NAMESPACE } from './constants';

const getSelectedOption = (state) =>
  _.get(state, ['request', 'options']).find(option => option.value === getRequestFor(state));
export const getSelectedType = (state) =>
  _.get(state, ['request', 'resourceTypes'])
    .find(resource => resource.id.toString() === getField(state, 'resourceType'));
export const selector = formValueSelector(REQUEST_FORM_NAMESPACE);
export const getSyncErrors = state => getFormSyncErrors(REQUEST_FORM_NAMESPACE)(state);
export const hasSyncErrors = state => _.isEmpty(getFormSyncErrors(REQUEST_FORM_NAMESPACE)(state));
export const getType = state => getField(state, 'resourceType');
export const getRequestFor = state => selector(state, 'requestFor');
export const getResourceTypes = state => _.get(state, ['request', 'resourceTypes'], [])
export const getResources = state => _.get(state, ['request', 'resources'], [])
export const getSelectedResourceType = (state, typeId = null) => getResourceTypes(state)
  .find(type => type.id.toString() === (typeId ? typeId : getType(state)));
export const getSelectedResources = state => getField(state, 'resources').filter(resource => resource.isSelected)
export const getRecipients = state => selector(state, 'recipients');
export const getField = (state, field) => selector(state, field);
export const getBreadCrumbs = state => [_.get(getSelectedOption(state), ['name'], ''), _.get(getSelectedType(state), ['name'], '')];

export const getRequestSubmitData = state => {
  const type = getSelectedResourceType(state);
  return {
    requestFor: getRequestFor(state),
    resourceType: {
      id: type.id,
      name: type.name
    },
    resources: getSelectedResources(state),
    recipients: getRecipients(state)
  }
}