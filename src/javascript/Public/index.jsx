import React from 'react';
import { connect } from 'react-redux';
import {Switch} from 'react-router-dom';

import PageShell from '../components/PageShell'

import {
  Route,
} from 'react-router-dom';
import NotFound from '../components/404';

import Login from './login';

const Public = () => {
  return (
    <Switch>
      <Route exact path="/" component={Login} />
      <Route path="/app-login" component={Login} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default connect()(Public);
