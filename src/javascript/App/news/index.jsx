import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { requestPostsData } from './actions';
import { getPosts } from './selectors';
import { getNewPosts } from './../library/selectors';

class NewsPage extends React.Component {
  componentDidMount() {
    this.props.requestPostsData();
  }
  goToPage(postId) {
    const { match, history } = this.props;
    history.push(`${match.path}/${postId}`);
  }
  render() {
    const { posts, newPosts } = this.props;
    return (
      <div className="page-transition">
        <div className="body">
          <div className="main">
            {
              posts && posts.map(post => {
                const styles = {};
                styles.backgroundImage = post.featured_image ? `url(${post.featured_image})` : '';
                const isNew = newPosts.includes(post.id);
                return (
                  <div style={styles} className="flex-item post" key={post.id} onClick={() => this.goToPage(post.id)}>
                    <span className="textoverlay">
                      {isNew && <span className="badge">new</span>}
                      <h2 className="title">{post.title.rendered}</h2>
                      <div dangerouslySetInnerHTML={{ __html: post.excerpt.rendered }}>
                      </div>
                    </span>
                  </div>
                );
              })
            }
          </div>
        </div>
      </div>
    );
  }
}
NewsPage.propTypes = {
  posts: PropTypes.array,
  requestPostsData: PropTypes.func,
  newPosts: PropTypes.array
};
const mapStateToProps = state => {
  return {
    posts: getPosts(state),
    newPosts: getNewPosts(state)
  };
};

export default connect(mapStateToProps, { requestPostsData })(NewsPage);
