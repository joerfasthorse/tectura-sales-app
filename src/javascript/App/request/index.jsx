import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import StepOne from './components/step-one';
import StepTwo from './components/step-two';
import RequestHeader from './components/request-header';
import RequestFooter from './components/request-footer';
import validate from './validate';
import { hasSyncErrors, getType, getBreadCrumbs } from './selectors';
import { REQUEST_FORM_NAMESPACE } from './constants';
import { requestResouceTypes, requestSubmit, setRequestFormStep, sumbitFormForValidate } from './actions';
import LoadingComponent from '../../components/loading'
const STEP_ONE = 1;
const STEP_TWO = 2;
const STEP_THREE = 3;

class RequestPage extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.requestResouceTypes();
  }

  render() {
    const { handleSubmit, submitForm, isSubmitting, breadcrumbs, currentStep } = this.props;
    return (
      <div className="page-transition">
        <form onSubmit={handleSubmit(submitForm)}>
          <div className="body requests">
            <RequestHeader breadcrumbs={breadcrumbs} currentStep={currentStep} />
            {isSubmitting ? <div className="main"><LoadingComponent /></div> : this.renderBody()}
          </div>
        </form>
      </div>

    );
  }
  renderBody() {
    const { currentStep, hasError } = this.props;
    return (
      <React.Fragment>
        {currentStep === STEP_ONE && <StepOne />}
        {currentStep === STEP_TWO && <StepTwo />}
        {currentStep === STEP_THREE && this.renderFormComfirmation()}
        <RequestFooter
          hasNext={this.hasNext(currentStep)}
          hasPrevious={this.hasPrevious(currentStep)}
          hasSend={this.hasSend(currentStep)}
          handleNext={this.handleNext}
          enableNext={this.enableNext(currentStep, hasError)}
          enableSend={this.enableSend(currentStep, hasError)}
          handlePrevious={this.handlePrevious}
          handleSend={this.handleSend}
        />
      </React.Fragment>
    )
  }
  renderFormComfirmation() {
    return (
      <div className="main success"><h2 className="confirmation">Request Sent Successfully!</h2></div>
    )
  }
  hasNext(currentStep) {
    return currentStep === STEP_ONE ? true : false;
  }
  hasPrevious(currentStep) {
    return currentStep === STEP_TWO ? true : false;
  }
  hasSend(currentStep) {
    return currentStep === STEP_TWO ? true : false;
  }
  enableNext(currentStep, hasError) {
    if (currentStep === STEP_ONE && hasError) {
      return true;
    }
    return false;
  }
  enableSend(currentStep, hasError) {
    if (currentStep === STEP_TWO && hasError) {
      return true;
    }
    return false;
  }
  handleNext = () => {
    const { setRequestFormStep, currentStep } = this.props;
    setRequestFormStep(currentStep + 1)
  }
  handlePrevious = () => {
    const { setRequestFormStep, currentStep } = this.props;
    setRequestFormStep(currentStep - 1)
  }
  handleSend = () => {
    this.props.requestSubmit({
      callback: () => {
        // console.log('submitted')
      }
    })

  }

}

RequestPage.propTypes = {
  hasError: PropTypes.bool,
  breadcrumb: PropTypes.array,
  typeName: PropTypes.string
};
const mapStateToProps = state => {
  return {
    hasError: hasSyncErrors(state),
    typeName: getType(state),
    breadcrumbs: getBreadCrumbs(state),
    isSubmitting: state.request.isSubmittingRequest,
    currentStep: state.request.requestFormStep
  };
};

const mapDispatchToProps = dispatch => {
  return {
    requestSubmit: (callback) => dispatch(requestSubmit(callback)),
    requestResouceTypes: () => dispatch(requestResouceTypes()),
    setRequestFormStep: data => dispatch(setRequestFormStep(data)),
    submitForm: data => dispatch(sumbitFormForValidate(data)),
  };
};

const connected = connect(mapStateToProps, mapDispatchToProps)(RequestPage);

const formed = reduxForm({
  form: REQUEST_FORM_NAMESPACE,
  destroyOnUnmount: false,
  initialValues: {
    recipients: [{}],
    resources: [{}]
  },
  validate
})(connected);

export default formed;